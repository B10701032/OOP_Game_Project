#include "Character.h"

Character::Character()
{
}

Character::Character(string name, int hp, int handCnt, vector<CharacterSkill> hand, int id)
	: name(name), hp(hp), handCnt(handCnt), hand(hand), id(id)
{
	this->alive = true;
	this->icon = 'A' + id; // 依照加入順序給予代號
	// 初始 handStatus
	handStatus = new int[this->handCnt];
	for (int i = 0; i < handCnt; i++)
		handStatus[i] = 1;
	this->shield = 0;
}

Character::~Character() // ++++something wrong++++
{
	//delete[] handStatus; 
}

void Character::setPosition(Point2d p)
{
	this->position = p;
}

void Character::setSlectedCardId(vector<int> v)
{
	this->selectedCardId = v;
}

bool Character::hasCard(int id) // 從卡片 id 判斷有沒有這張卡片
{
	for (int i = 0; i < this->handCnt; i++)
	{
		if (hand[i].getCardId() == id)
			return true;
	}
	return false;
}

int Character::findCardPos(int id) // 從卡片 id 找卡片在手牌的位置
{
	for (int i = 0; i < this->handCnt; i++)
	{
		if (hand[i].getCardId() == id)
			return i;
	}
	return -1;
}

void Character::clearSelectedCardId()
{
	this->selectedCardId.clear();
}

void Character::check() // 檢查角色手上牌的狀況
{
	int h = 0, d = 0; // 先數個有幾張
	for (int i = 0; i < this->handCnt; i++)
	{
		if (this->handStatus[i] == 1)
			h += 1;
		if (this->handStatus[i] == 0)
			d += 1;
	}

	cout << "hand: ";
	for (int i = 0; i < this->handCnt; i++)
	{
		if (this->handStatus[i] == 1)
		{
			cout << hand[i].getCardId();
			if (h - 1 != 0)
				cout << ", ";
			h -= 1;
		}
	}
	cout << "; discard: ";
	for (int i = 0; i < this->handCnt; i++)
	{
		if (this->handStatus[i] == 0)
		{
			cout << hand[i].getCardId();
			if (d - 1 != 0)
				cout << ", ";
			d -= 1;
		}
	}
	cout << "\n"; // 一定要換行
}

void Character::useCard(int id) 
{
	int pos = findCardPos(id);
	this->handStatus[pos] = 0; 
}

void Character::delCard(int id)
{
	vector<CharacterSkill> temp;
	for (int i = 0; i < handCnt; i++)
	{
		if (hand[i].getCardId() != id)
		{
			temp.push_back(hand[i]);
		}
	}
	handCnt -= 1;
	hand.clear();
	hand = temp;
}

void Character::resetHandStatus() // 刪除要丟棄的牌後，其餘的牌移到 hand 中
{
	for (int i = 0; i < handCnt; i++)
		handStatus[i] = 1;
}

bool Character::canRest() // 檢查角色是否能長休
{
	bool result = true;

	int cnt = 0; // 算棄牌堆有幾張牌
	for (int i = 0; i < hand.size(); i++)
	{
		if (handStatus[i] != 1) // 如果卡片不可使用
			cnt += 1;
	}

	if (cnt == 0) // 不能棄牌
		result = false;

	return result;
}

bool Character::canPlay() // 可以出牌
{
	bool result = true;
	int cnt = 0; // 算手牌有幾張
	for (int i = 0; i < hand.size(); i++)
	{
		if (handStatus[i] == 1) // 如果卡片可使用
			cnt += 1;
	}
	if (cnt < 2) // 沒有牌可以出 
		result = false;
	return result;
}