#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "CharacterData.h"
#include "CharacterSkill.h"
#include "Point2d.h"

class Character
{
public:
	Character();
	Character(string name, int hp, int handCnt, vector<CharacterSkill> hand, int id);
	~Character(); // ++++something wrong++++
	void setPosition(Point2d p);
	void setSlectedCardId(vector<int> v); 
	void setAgiFirst(int agi) { this->agiFirst = agi; }
	void setAgiSecond(int agi) { this->agiSecond = agi; }
	void setShield(int newS) { this->shield = newS; }
	void setHp(int newHp) { this->hp = newHp; }
	string getName() { return this->name; }
	int getHp() { return this->hp; }
	int getHandCnt() { return this->handCnt; }
	vector<CharacterSkill> getHand() { return this->hand; }
	vector<int> getSelectedCardId() { return this->selectedCardId; }
	int getId() { return this->id; }
	char getIcon() { return this->icon; }
	int getAgiFirst() { return this->agiFirst; }
	int getAgiSecond() { return this->agiSecond; }
	bool isAlive() { return this->alive; }
	int* getHandStatus() { return handStatus; }
	Point2d getPosition() { return position; }
	int getShield() { return this->shield; }
	void isAttacked(int val) { this->hp -= val; }
	void isKilled() { this->alive = false; }
	bool hasCard(int id); // 從卡片 id 判斷有沒有這張卡片
	int findCardPos(int id); // 從卡片 id 找卡片在手牌的位置，如果沒有，回傳 -1
	void clearSelectedCardId(); // 清空每一輪選擇要打的牌
	void check(); // 檢查角色手上牌的狀況
	void useCard(int id); // 將卡片改為已經用過 (id 和實際在hand的位置可能不一樣)
	void delCard(int id);
	void resetHandStatus(); // 刪除要丟棄的牌後，其餘的牌移到 hand 中
	bool canRest(); // 檢查角色是否能長休
	bool canPlay(); // 可以出牌

private:
	string name;
	int hp;
	int handCnt; // 手牌數量
	vector<CharacterSkill> hand; // 手牌
	int id; // 加入的順序 (從 0 開始)
	char icon; // 在地圖上的符號;
	bool alive; // 角色是否還活著
	int* handStatus; // 記錄每張牌的狀況 (-1 刪除, 0 棄牌堆, 1 可使用)
	Point2d position;
	vector<int> selectedCardId; // 決定每一輪要打的牌 (長休也在這)
	int agiFirst; // 角色每一輪的敏捷值
	int agiSecond; // 比較 agi 的 agi
	int shield; // 角色每一輪的護甲值
};