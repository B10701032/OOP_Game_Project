#include "CharacterData.h"

CharacterData::CharacterData() : hp(0), startCardCnt(0), allCardCnt(0)
{
}

CharacterData::CharacterData(string name, int hp, int startCardCnt, int allCardCnt, vector<CharacterSkill> allCards)
	: name(name), hp(hp), startCardCnt(startCardCnt), allCardCnt(allCardCnt), allCards(allCards)
{
}