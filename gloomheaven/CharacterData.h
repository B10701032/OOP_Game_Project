#pragma once
#include <iostream>
#include <string>
#include "CharacterSkill.h"
#include <iostream>
#include <string>
#include <vector>
using namespace std;

class CharacterData
{
public:
	CharacterData();
	CharacterData(string name, int hp, int startCardCnt, int allCardCnt, vector<CharacterSkill> allCards);
	void setHp(int newHp) { this->hp = newHp; }
	string getName() { return name; }
	int getHp() { return hp; }
	int getStartCardCnt() { return startCardCnt; }
	int getAllCardCnt() { return allCardCnt; }
	vector<CharacterSkill> getAllCards() { return allCards; }
private:
	string name;
	int hp;
	int startCardCnt;
	int allCardCnt;
	vector<CharacterSkill> allCards;
};