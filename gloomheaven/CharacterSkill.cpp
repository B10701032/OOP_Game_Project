#include "CharacterSkill.h"

CharacterSkill::CharacterSkill()
{
}

CharacterSkill::CharacterSkill(string cardId, string agility, string firstSkill, string secondSkill)
	: firstSkill(firstSkill), secondSkill(secondSkill)
{
	// 把 cardId 和 agility 從 str 轉 int
	this->cardId = stringToInteger(cardId);
	this->agility = stringToInteger(agility);
}

CharacterSkill::CharacterSkill(string cardDescription)
{
	istringstream iss(cardDescription);

	string tempId, tempAgi, tempFirstSkill, tempSecondSkill;
	iss >> tempId >> tempAgi;
	iss.clear();
	iss.str("");
	
	size_t agiPos = cardDescription.find(tempAgi);
	size_t hyphenPos = cardDescription.find("-");
	tempFirstSkill = cardDescription.substr(agiPos + 3, hyphenPos - agiPos - 4);
	tempSecondSkill = cardDescription.substr(hyphenPos + 2);

	this->cardId = stringToInteger(tempId);
	this->agility = stringToInteger(tempAgi);
	this->firstSkill = tempFirstSkill;
	this->secondSkill = tempSecondSkill;	
	setFirstSkills(this->firstSkill);
	setSecondSkills(this->secondSkill);
}

void CharacterSkill::setFirstSkills(string str)
{
	istringstream iss(str);
	string tempWord;

	while (iss >> tempWord)
	{
		string tempSkill;

		while (tempWord == "attack") // 避免連續多個 attack 敘述
		{
			tempSkill += tempWord;
			iss >> tempWord;
			tempSkill += " " + tempWord;


			if (iss >> tempWord)
			{
				if (tempWord == "range") // attack x range y
				{
					tempSkill += " " + tempWord;
					iss >> tempWord;
					tempSkill += " " + tempWord;
					this->firstSkills.push_back(tempSkill);
				}

				else // 下一個 word 是 move / shield / heal
				{
					this->firstSkills.push_back(tempSkill);
					tempSkill = "";
				}
			}

			else // 如果已經讀到尾端
			{
				this->firstSkills.push_back(tempSkill);
				tempWord = "";
			}
		}

		if (tempWord == "move" || tempWord == "shield" || tempWord == "heal")
		{
			tempSkill += tempWord;
			iss >> tempWord;
			tempSkill += " " + tempWord;
			this->firstSkills.push_back(tempSkill);
		}
	}

	iss.clear();
	iss.str("");
}

void CharacterSkill::setSecondSkills(string str)
{
	istringstream iss(str);
	string tempWord;

	while (iss >> tempWord)
	{
		string tempSkill;

		while (tempWord == "attack") // 避免連續多個 attack 敘述
		{
			tempSkill += tempWord;
			iss >> tempWord;
			tempSkill += " " + tempWord;


			if (iss >> tempWord)
			{
				if (tempWord == "range") // attack x range y
				{
					tempSkill += " " + tempWord;
					iss >> tempWord;
					tempSkill += " " + tempWord;
					this->secondSkills.push_back(tempSkill);
				}

				else // 下一個 word 是 move / shield / heal
				{
					this->secondSkills.push_back(tempSkill);
					tempSkill = "";
				}
			}

			else // 如果已經讀到尾端
			{
				this->secondSkills.push_back(tempSkill);
				tempWord = "";
			}
		}

		if (tempWord == "move" || tempWord == "shield" || tempWord == "heal")
		{
			tempSkill += tempWord;
			iss >> tempWord;
			tempSkill += " " + tempWord;
			this->secondSkills.push_back(tempSkill);
		}
	}

	iss.clear();
	iss.str("");
}


void CharacterSkill::print()
{
	cout << this->cardId << " " << this->agility << " "
		<< this->firstSkill << " - " << this->secondSkill << endl;
}