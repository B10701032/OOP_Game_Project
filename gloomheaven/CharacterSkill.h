#pragma once
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include "HelperFunction.h" // stringToInt()
using namespace std;

class CharacterSkill
{
public:
	CharacterSkill();
	CharacterSkill(string cardId, string agility, string firstSkill, string secondSkill);
	CharacterSkill(string cardDescription);
	void setFirstSkills(string str);
	void setSecondSkills(string str);
	int getCardId() { return this->cardId; }
	int getAgility() { return this->agility; }
	string getFirstSkill() { return this->firstSkill; }
	string getSecondSkill() { return this->secondSkill; }
	vector<string> getFirstSkills() { return this->firstSkills; }
	vector<string> getSecondSkills() { return this->secondSkills; }
	void print(); // 印出技能卡資料
private:
	int cardId; // 卡片編號
	int agility; // 敏捷值
	string firstSkill; // 上半部技能敘述 
	string secondSkill; // 下半部技能敘述 
	// ...技能敘述 1, 技能敘述 2, ...... QQ
	// 加了 s 的是一個個敘述.....
	vector<string> firstSkills;
	vector<string> secondSkills;
};

