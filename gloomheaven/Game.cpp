#include "Game.h"
using namespace std;
Game::Game()
{

}

Game::Game(string characterFileName, string monsterFileName, bool debugMode)
	: characterFileName(characterFileName), monsterFileName(monsterFileName), debugMode(debugMode)
{
	characterWin = true;
	setAllCharacterData();
	this->allCharacterCnt = this->allCharacterData.size();
	setAllMonsterData();
	this->allMonsterCnt = this->allMonsterData.size();
	this->roundCnt = 0;
	this->battling = true;
}

void Game::startGame() // 開啟一場新的遊戲 (遊戲流程)
{
	srand(time(nullptr)); // 怪物抽牌用，程式一開始執行一次就好
	setCharacterJoinCnt(); // 決定出場角色數量
	setCharacterJoin(); // 決定出場角色與技能卡
	setMap(); // 決定地圖名和初始化地圖
	setMonsterOnMap(); // 設定出場怪物資訊
	setCharacterPosition(); // 選擇角色起始位置

	bool battling = true;
	while (battling)
	{
		cout << "round " << this->roundCnt + 1 << ":\n";
		updateCharacterJoin(); //檢查是否有角色死亡，如果有，輸出更新後的地圖
		if (characterJoin.empty()) // 如果更新後 vector 是空的
		{
			characterWin = false; // 判斷怪物贏
			battling = false; // 結束戰鬥
			break;
		}

		setCharacterSkill(); // 決定每輪角色要打的牌
		if (this->debugMode == 1)
			setMonsterSkill_DEBUG();
		else
			setMonsterSkill();

		//cout << "\nSort by AGI: \n";
		sortByAgi(); // 排序並顯示所有角色與怪物的行動先後順序
		showTurn();
		//cout << "\nBattle: \n";
		battle(); // 依照 agi 戰鬥 (bool)，含決定開門的部分
		//cout << "\n";

		updateAllEntity(); // 刪除戰鬥後死亡的角色和怪物 
		openDoor(); // 每輪結束開門 (若有打開門則更新地圖, 反之不用)

		if (characterJoin.empty())
		{
			characterWin = false; // 判斷怪物贏
			battling = false; // 結束戰鬥
			break;
		}
		if (monsterOnMap.empty())
		{
			// 判斷角色贏
			battling = false; // 結束戰鬥
			break;
		}

		this->roundCnt++;
	}
	showWinner(); // 螢幕上印出角色或怪物獲勝
}

void Game::startGameTest() // DEBUG 用
{
	this->map = Map("maptest.txt");
	this->map.openArea(Point2d(10, 4), Point2d(10, 4));// 打開地圖下半部

	// 創角色基本資料
	string name_C = "brute";
	int pos = findCharacterDataPos(name_C, this->allCharacterData);
	int hp_C = allCharacterData[pos].getHp();
	int handCnt_C = allCharacterData[pos].getStartCardCnt();
	vector<int> cardIds = { 0, 1, 2, 3, 4, 5 };
	vector<CharacterSkill> hand_C;
	for (int i = 0; i < handCnt_C; i++)
	{
		vector<CharacterSkill> allCards = allCharacterData[pos].getAllCards();
		hand_C.push_back(allCards[cardIds[i]]);
	}
	
	// 把角色 push back 進去 characterJoin
	characterJoin.push_back(Character(name_C, hp_C, handCnt_C, hand_C, 0)); // 創建兩個角色
	characterJoin.push_back(Character(name_C, hp_C, handCnt_C, hand_C, 1)); // 尾端 0 1 表在 vector 中的順序
	characterJoin[0].setPosition(Point2d(9, 4)); // 角色 A 的位置
	this->map.draw(Point2d(9, 4), characterJoin[0].getIcon());
	characterJoin[1].setPosition(Point2d(10, 3)); // 角色 B 的位置
	this->map.draw(Point2d(10, 3), characterJoin[1].getIcon());
	// 手動初始 AGI
	characterJoin[0].setAgiFirst(0);
	characterJoin[0].setAgiSecond(0);
	characterJoin[1].setAgiFirst(0);
	characterJoin[1].setAgiSecond(0);
	sortByAgi();

	// 創怪物基本資料
	string name_M = "guard";
	MonsterData data = findMonsterData(name_M, this->allMonsterData);
	string level_M = "base";
	int hp_M = data.getBaseHp();
	int damage_M = data.getBaseDamage();
	int range_M = data.getBaseRange();
	int handCnt_M = data.getAllCardCnt();
	vector<MonsterSkill> hand_M = data.getAllCards();
	
	// 把怪物 push back 進去 MonsterOnMap
	monsterOnMap.push_back(Monster(name_M, level_M, hp_M, damage_M, range_M, handCnt_M, hand_M, 0, Point2d(7, 4))); // 0 是在 vector 中的位置，Point2d 是位置
	this->map.draw(Point2d(7, 4), monsterOnMap[0].getIcon());

	map.display();

	bool testing = true;
	while (testing)
	{
		string str;
		getline(cin, str);

		if (str == "exit") testing = false;
		else if (str.find("move") != string::npos) // 移動
		{
			if (isupper(str[5]))  // move A 3
			{
				char icon = str[5];
				Character& c = findCharacter(icon, characterJoin);
				string maxStr = str.substr(7);
				int max = stringToInteger(maxStr);
				move(c, max);
			}
			else // move a wasd
			{
				char icon = str[5];
				Monster& m = findMonster(icon, monsterOnMap);
				string direction = str.substr(7);
				move(m, direction);
			}
		}
		else if (str.find("attack") != string::npos) // 攻擊
		{
			if (isupper(str[0])) // A attack 1 (1是近距離攻擊)
			{
				char icon = str[0];
				Character& c = findCharacter(icon, characterJoin);
				string rangeStr = str.substr(9);
				int range = stringToInteger(rangeStr);
				char targetIcon = findTarget(c, range);
				if (targetIcon == '0')
				{
					cout << "no one lock\n";
				}
				else
				{
					// 被攻擊的怪物扣掉血量
					Monster& t = findMonster(targetIcon, monsterOnMap);
					t.isAttacked(100); // 這裡假設若可攻擊，則怪物必死
					cout << c.getIcon() << " attack " << t.getIcon() << " " << 100 << " damage, "
						<< t.getIcon() << " shield " << t.getShield() << ", "
						<< t.getIcon() << " remain " << t.getHp() << " hp\n";

					// 檢查被攻擊對象是否死亡
					if (t.getHp() <= 0)
					{
						t.isKilled();
						cout << t.getIcon() << " is killed!!\n";
						// 將死亡對象從地圖上清除
						this->map.erase(t.getPosition());
						// 輸出更新後的地圖
						this->map.display();
						updateAllEntity(); // 刪除戰鬥後死亡的角色和怪物 
					}
				}
			}

			else // a attack 1
			{
				char icon = str[0];
				Monster& m = findMonster(icon, monsterOnMap);
				string rangeStr = str.substr(9);
				int range = stringToInteger(rangeStr);
				char targetIcon = findTarget(m, range);

				if (targetIcon == '0')
				{
					cout << "no one lock\n";
				}
				else
				{
					Character& t = findCharacter(targetIcon, characterJoin);

					// 被攻擊的人扣掉血量
					t.isAttacked(100); // 這裡假設若可攻擊，則怪物必死
					cout << m.getIcon() << " attack " << t.getIcon() << " " << 100 << " damage, "
						<< t.getIcon() << " shield " << t.getShield() << ", "
						<< t.getIcon() << " remain " << t.getHp() << " hp\n";
					// 檢查被攻擊對象是否死亡
					if (t.getHp() <= 0)
					{
						t.isKilled();
						cout << t.getIcon() << " is killed!!\n";
						// 將死亡對象從地圖上清除
						this->map.erase(t.getPosition());
						// 輸出更新後的地圖
						this->map.display();
						updateAllEntity(); // 刪除戰鬥後死亡的角色和怪物 
					}
				}
			}
		}
		else if (str.find("agi") != string::npos) // A agi 18
		{
			char icon = str[0];
			Character& c = findCharacter(icon, characterJoin);
			string vStr = str.substr(6);
			int v = stringToInteger(vStr);
			c.setAgiFirst(v);
			c.setAgiSecond(v);
			sortByAgi();
		}

		openDoor(); // 每輪結束開門 (若有打開門則更新地圖, 反之不用)
	}	
}

void Game::setAllCharacterData()
{
	ifstream inStream;
	inStream.open(characterFileName);

	if (inStream.fail())
	{
		cout << "error: fail to open " << characterFileName << endl
			<< "\tplease exit the program and check the txt file\n\n";
		
	}
	else
	{
		vector<CharacterData> tempCharacterData;

		int characterCnt = 0;
		inStream >> characterCnt;
		for (int i = 0; i < characterCnt; i++)
		{
			string name;
			int hp = 0, startCardCnt = 0, allCardCnt = 0;
			inStream >> name >> hp >> startCardCnt >> allCardCnt;
			inStream.ignore(); // 避免誤讀 cin 後的空白

			vector<CharacterSkill> allCards;
			for (int i = 0; i < allCardCnt; i++)
			{
				string description;
				getline(inStream, description);
				allCards.push_back(CharacterSkill(description));
			}

			tempCharacterData.push_back(CharacterData(name, hp, startCardCnt, allCardCnt, allCards));
		}

		this->allCharacterData = tempCharacterData;
	}

	inStream.close();
}

void Game::setAllMonsterData() 
{
	ifstream inStream;
	inStream.open(this->monsterFileName);

	if (inStream.fail())
	{
		cout << "error: fail to open " << this->monsterFileName << endl
			<< "\tplease exit the program and check the txt file\n\n";
	}

	else
	{
		vector<MonsterData> tempMonsterData;

		int monsterCnt = 0;
		inStream >> monsterCnt;
		for (int i = 0; i < monsterCnt; i++)
		{
			string name;
			int baseHp = 0, baseDamage = 0, baseRange = 0, 
				eliteHp = 0, eliteDamage = 0, eliteRange = 0;
			inStream >> name >> baseHp >> baseDamage >> baseRange
				>> eliteHp >> eliteDamage >> eliteRange;
			inStream.ignore(); // 避免誤讀 cin 後的空白

			int allCardCnt = 6; // 怪物有 6 張技能卡
			vector<MonsterSkill> allCards;
			for (int i = 0; i < allCardCnt; i++)
			{
				string description;
				getline(inStream, description);
				allCards.push_back(MonsterSkill(description));
			}

			tempMonsterData.push_back(MonsterData(name, baseHp, baseDamage, baseRange,
				eliteHp, eliteDamage, eliteRange, allCards));
		}

		this->allMonsterData = tempMonsterData;
	}

	inStream.close();
}

void Game::setCharacterJoinCnt() // 決定出場角色數量
{
	string cntStr; // 先以字串讀取輸入，避免不合規定的輸入
	bool vaildCommand = false;
	while (!vaildCommand) // 若使用者不是輸入 play 或 exit，要求重新輸入
	{
		cout << "請輸入出場角色數量:\n";
		getline(cin, cntStr);
		if (cntStr == "2" || cntStr == "3" || cntStr == "4")
			vaildCommand = true;
		else
			cout << "error command: please enter the number of character in game (2 ~ 4)\n";
	}

	this->characterJoinCnt = stringToInteger(cntStr);
}

void Game::setCharacterJoin()
{
	int cnt = this->characterJoinCnt;
	for (int i = 0; i < cnt; i++)
	{
		string name;
		int hp, handCnt;
		vector<CharacterSkill> hand;

		string command; // 先以字串讀取輸入，避免不合規定的輸入
		bool vaildCommand = false;
		while (!vaildCommand) // 若使用者不是輸入 play 或 exit，要求重新輸入
		{
			getline(cin, command);
			istringstream iss(command);
			iss >> name;

			// 確認名字存在於所有可選擇的角色中
			int pos = findCharacterDataPos(name, this->allCharacterData);
			if (pos == -1)
			{
				cout << "error command: name does not exist\n";
				iss.clear();
				iss.str("");
				continue;
			}
			
			// 取得 hp
			hp = allCharacterData[pos].getHp();
			// 取得可帶出場的卡牌數
			handCnt = allCharacterData[pos].getStartCardCnt();

			// 防止輸入超過數量的手牌
			vector<int> cardIds;
			int cardId = 0;
			while (iss >> cardId)
				cardIds.push_back(cardId);

			if (cardIds.size() != handCnt)
			{
				if (cardIds.size() > handCnt)
					cout << "error command: choose too many cards\n";
				else
					cout << "error command: choose too few cards\n";
				iss.clear();
				iss.str("");
				continue;
			}

			// 檢查輸入的編號是否重複輸入或超出 (所有可選擇的卡片) 範圍
			int idBound = allCharacterData[pos].getAllCardCnt();
			bool vaildInput = true;
			bool* vaildId = new bool[idBound];
			for (int i = 0; i < handCnt; i++)
			{
				int id = cardIds[i];
				
				if (id >= idBound) // 超出範圍
				{
					cout << "error command: card " << id << " does not exist\n";
					vaildInput = false;
					break;
				}

				if (vaildId[id] == 1) // 重複輸入
				{
					cout << "error command: card " << id << " is duplicate\n";
					vaildInput = false;
					break;
				}

				vaildId[id] = 1;
			}

			delete[] vaildId;
			if (!vaildInput) // 輸入有問題
			{
				iss.clear();
				iss.str("");
				continue;
			}

			// 輸入合法
			for (int i = 0; i < handCnt; i++)
			{
				vector<CharacterSkill> allCards = allCharacterData[pos].getAllCards();
				hand.push_back(allCards[cardIds[i]]);
			}
		
			iss.clear();
			iss.str("");
			vaildCommand = true;
		}

		this->characterJoin.push_back(Character(name, hp, handCnt, hand, i));
	}
}

void Game::setMap()
{
	ifstream inStream;
	string fileName;
	bool vaildCommand = false;
	while (!vaildCommand)
	{
		getline(cin, fileName);
		inStream.open(fileName);
		if (inStream.fail())
		{
			cout << "error command: fail to open " << fileName << endl
				<< "\tplease enter the file name again" << endl;
		}
		else
			vaildCommand = true;

		inStream.close();
	}
	this->map = Map(fileName);
}

void Game::setMonsterOnMap()
{
	vector<string> info = this->map.getMonsterInfo();
	int monsterCnt = this->map.getMaxMonsterCnt();
	int characterCnt = this->characterJoinCnt;
	for (int i = 0; i < monsterCnt; i++)
	{
		string name, level;
		int x = 0, y = 0;
		int s2 = 0, s3 = 0, s4 = 0;
		istringstream iss(info[i]);
		iss >> name >> x >> y >> s2 >> s3 >> s4;

		Point2d position(y, x); // x為column、y為row // 為什麼要這樣 QQ
		MonsterData m = findMonsterData(name, this->allMonsterData);
		int hp = 0, damage = 0, range = 0;
		int handCnt = m.getAllCardCnt();
		vector<MonsterSkill> hand = m.getAllCards();

		if (characterCnt == 2 && s2 != 0) // 怪物不出現就不用加了，所以怪物的 id 可能跳號
		{
			if (s2 == 1) // 普通
			{
				level = "base";
				hp = m.getBaseHp();
				damage = m.getBaseDamage();
				range = m.getBaseRange();
			}
			else // 菁英
			{
				level = "elite";
				hp = m.getEliteHp();
				damage = m.getEliteDamage();
				range = m.getEliteRange();
			}
			this->monsterOnMap.push_back(Monster(name, level, hp, damage, range, handCnt, hand, i, position));
		}
		else if (characterCnt == 3 && s3 != 0)
		{
			if (s3 == 1) // 普通
			{
				level = "base";
				hp = m.getBaseHp();
				damage = m.getBaseDamage();
				range = m.getBaseRange();
			}
			else // 菁英
			{
				level = "elite";
				hp = m.getEliteHp();
				damage = m.getEliteDamage();
				range = m.getEliteRange();
			}
			this->monsterOnMap.push_back(Monster(name, level, hp, damage, range, handCnt, hand, i, position));
		}
		else if (characterCnt == 4 && s4 != 0)
		{
			if (s4 == 1) // 普通
			{
				level = "base";
				hp = m.getBaseHp();
				damage = m.getBaseDamage();
				range = m.getBaseRange();
			}
			else // 菁英
			{
				level = "elite";
				hp = m.getEliteHp();
				damage = m.getEliteDamage();
				range = m.getEliteRange();
			}
			this->monsterOnMap.push_back(Monster(name, level, hp, damage, range, handCnt, hand, i, position));
		}

		iss.clear();
		iss.str("");
	}

	// 把怪物們的位置印到地圖上
	for (int i = 0; i < this->monsterOnMap.size(); i++)
		this->map.draw(this->monsterOnMap[i].getPosition(), this->monsterOnMap[i].getIcon());
}

void Game::setCharacterPosition() // 選擇角色起始位置
{
	vector<Point2d> p = this->map.getInitialPosition(); // 複製一個 local 的起始位置供刪除及使用
	this->map.openArea(p[0], p[0]); // 開啟這個區域
	for (int i = 0; i < p.size(); i++) // 印到地圖上
	{
		if (i == 0) // 最前面的是最小的位置
			this->map.draw(p[i], '*');
		else
			this->map.draw(p[i], '_');
	}
	this->map.display();

	for (int i = 0; i < this->characterJoinCnt; i++)
	{
		string command;
		bool notSelected = true;
		while (notSelected)
		{
			Point2d pos = p[0]; // 從第一個可選擇的起始位置做基準 (放這裡輸入錯誤才能恢復起點)
			getline(cin, command);
			for (int j = 0; j < command.length(); j++)
			{
				if (command[j] == 'e')
				{
					notSelected = false;
					break;
				}
				else if (command[j] == 'w')
				{
					bool vaild = false;
					for (int k = 0; k < this->map.getInitialPosition().size(); k++)
					{
						if (Point2d(pos.getX() - 1, pos.getY()) == this->map.getInitialPosition()[k]) // 和原本的起始位置比
						{
							vaild = true;
							break;
						}
					}
					if (vaild)
					{
						pos = Point2d(pos.getX() - 1, pos.getY());
						notSelected = false;
					}
				}
				else if (command[j] == 'a')
				{
					bool vaild = false;
					for (int k = 0; k < this->map.getInitialPosition().size(); k++)
					{
						if (Point2d(pos.getX(), pos.getY() - 1) == this->map.getInitialPosition()[k])
						{
							vaild = true;
							break;
						}
					}
					if (vaild)
					{
						pos = Point2d(pos.getX(), pos.getY() - 1);
						notSelected = false;
					}
				}
				else if (command[j] == 's')
				{
					bool vaild = false;
					for (int k = 0; k < this->map.getInitialPosition().size(); k++)
					{
						if (Point2d(pos.getX() + 1, pos.getY()) == this->map.getInitialPosition()[k])
						{
							vaild = true;
							break;
						}
					}
					if (vaild)
					{
						pos = Point2d(pos.getX() + 1, pos.getY());
						notSelected = false;
					}
				}
				else if (command[j] == 'd')
				{
					bool vaild = false;
					for (int k = 0; k < this->map.getInitialPosition().size(); k++)
					{
						if (Point2d(pos.getX(), pos.getY() + 1) == this->map.getInitialPosition()[k])
						{
							vaild = true;
							break;
						}
					}
					if (vaild)
					{
						pos = Point2d(pos.getX(), pos.getY() + 1);
						notSelected = false;
					}
				}
			}

			if (!notSelected)
			{
				bool vaild = false;
				for (int k = 0; k < p.size(); k++) // 不能選已經被選走的位置
				{
					if (Point2d(pos.getX(), pos.getY()) == p[k])
					{
						vaild = true;
						break;
					}
				}
				if (vaild)
				{
					this->characterJoin[i].setPosition(pos);
					delPoint2d(p, pos);
					this->map.draw(pos, this->characterJoin[i].getIcon());
				}
				else
				{
					cout << "error move!!!\n";
					notSelected = true;
				}
			}

			// 每次輸入皆要更新
			if (!notSelected && i == this->characterJoinCnt - 1) // 設定完畢，地圖改回原本的樣子
			{
				for (int i = 0; i < p.size(); i++)
				{
					int x = p[i].getX();
					int y = p[i].getY();
					char icon = this->map.getDefaultMap()[x][y];
					this->map.draw(p[i], icon);
				}
			}

			else // 更新可選擇的位置
			{
				for (int i = 0; i < p.size(); i++) // 印到地圖上
				{
					if (i == 0) // 最前面的是最小的位置
						this->map.draw(p[i], '*');
					else
						this->map.draw(p[i], '_');
				}
			}

			this->map.display();
		}
	}
}

void Game::setCharacterSkill() // 決定每輪角色要打的牌
{
	for (int i = 0; i < this->characterJoinCnt; i++) // 清空所有角色上一輪選的卡牌和 agi值
	{
		this->characterJoin[i].clearSelectedCardId();
		this->characterJoin[i].setAgiFirst(0);
		this->characterJoin[i].setAgiSecond(0);
	}

	bool* selected = new bool[this->characterJoinCnt]; // 記錄哪些角色已選過
	for (int i = 0; i < this->characterJoinCnt; i++)
		selected[i] = false;
	bool selecting = true; // 如果還有角色沒選到卡牌
	while (selecting)
	{
		string input;
		getline(cin, input);
		char name = input[0];
		bool notVaildName = true;
		for (int i = 0; i < this->characterJoinCnt; i++)
		{
			if (this->characterJoin[i].getIcon() == name)
				notVaildName = false;
		}
		if (notVaildName || input.find(" ") != 1) // 名字不存在
		{
			cout << "error: name " << input.substr(0, input.find(" ")) << " does not exist\n";
			continue;
		}
		else
		{
			Character p = findCharacter(name, this->characterJoin);
			string command = input.substr(2);
			if (command == "check")
			{
				p.check();
			}
			else if	(selected[name - 'A'] == true) // 如果該角色已經選過了
			{
				cout << "error: character " << name << " has seledcted\n";
			}
			else if (command == "-1")
			{
				// 先檢查角色能不能長休
				vector<int> temp;
				temp.push_back(-1);
				this->characterJoin[name - 'A'].setSlectedCardId(temp);
				this->characterJoin[name - 'A'].setAgiFirst(99);
				this->characterJoin[name - 'A'].setAgiSecond(99);
				selected[name - 'A'] = true;
			}
			else
			{
				bool vaild = true;
				istringstream iss(command);
				int id = 0;
				vector<int> temp;
				while (iss >> id)
					temp.push_back(id);
				if (temp.size() != 2)
				{
					cout << "error: please enter 2 card ID / -1 / check\n";
					vaild = false;
					continue;
				}
				if (temp[0] == temp[1])
				{
					cout << "error: cannot choose the same card\n";
					vaild = false;
					continue;
				}

				for (int i = 0; i < temp.size(); i++)
				{
					if (!p.hasCard(temp[i])) // 確認此角色有這張卡
					{
						cout << "error: card " << temp[i] << " does not exit\n";
						vaild = false;
						continue;
					}

					int cardPos = p.findCardPos(temp[i]);
					int* handStatus = p.getHandStatus();
					if (!handStatus[cardPos]) // 如果卡片已經用過 1 是可以用
					{
						cout << "error: card " << temp[i] << " has used\n";
						vaild = false;
						continue;
					}
				}

				if (vaild)
				{
					this->characterJoin[name - 'A'].setSlectedCardId(temp);
					int cardPosFirst = p.findCardPos(temp[0]); // 輸入的第一張卡片做為 agi 值
					int agiFirst = p.getHand()[cardPosFirst].getAgility();
					this->characterJoin[name - 'A'].setAgiFirst(agiFirst);
					int cardPosSecond = p.findCardPos(temp[1]);
					this->characterJoin[name - 'A'].setAgiSecond(cardPosSecond);


					selected[name - 'A'] = true;
				}
			}
		}

		int cnt = 0; // 數幾個玩家選過了
		for (int i = 0; i < this->characterJoinCnt; i++)
		{
			if (selected[i] == true)
				cnt += 1;
		}
		if (cnt == characterJoinCnt) // 所有角色皆選擇完畢
			selecting = false;
	}

	delete[] selected;
}

void Game::setMonsterSkill_DEBUG() // DEBUG_MODE = 1, 怪物的行動卡為依序出牌, 
{
	// 第一輪打編號0, 第二輪打出編號1..., 
	// 若出現重洗標誌則將棄牌堆洗回去後, 重新從編號0開始
	for (int i = 0; i < this->monsterOnMap.size(); i++)
	{
		int cnt = this->monsterOnMap[i].getHandCnt();
		vector<MonsterSkill> hand = this->monsterOnMap[i].getHand();
		int* handStatus = this->monsterOnMap[i].getHandStatus();
		for (int j = 0; j < cnt; j++)
		{
			if (handStatus[j] == 1)
			{
				this->monsterOnMap[i].setSelectedCardId(j);
				int agi = hand[j].getAgility();
				this->monsterOnMap[i].setAgi(agi);
				break;
			}
		}
	}
}

void Game::setMonsterSkill() // DEBUG_MODE = 0, 為所有已出現的怪物種類隨機抽一張行動卡
{
	int monsterCnt = this->monsterOnMap.size();
	bool* selected = new bool[monsterCnt];
	for (int i = 0; i < monsterCnt; i++)
		selected[i] = false;
	
	for (int i = 0; i < this->monsterOnMap.size(); i++)
	{
		if (!selected[i])
		{
			int rn = rand() % this->monsterOnMap[i].getHandCnt();
			bool notVaild = true;
			while (notVaild)
			{
				int* status = this->monsterOnMap[i].getHandStatus();
				if (status[rn] == 1) // 表示可以用
					break;
				else
					rn = rand() % this->monsterOnMap[i].getHandCnt();
			}

			this->monsterOnMap[i].setSelectedCardId(rn);
			vector<MonsterSkill> hand = this->monsterOnMap[i].getHand();
			int agi = hand[rn].getAgility();
			this->monsterOnMap[i].setAgi(agi);
			selected[i] = true;

			// 其他一樣種類的做同樣設定
			for (int j = 0; j < monsterCnt; j++)
			{
				if (this->monsterOnMap[j].getName() == this->monsterOnMap[i].getName() && i != j)
				{
					this->monsterOnMap[j].setSelectedCardId(rn);
					this->monsterOnMap[j].setAgi(agi);
					selected[j] = true;
				}
			}
		}
	}
	delete[] selected;
}

void Game::sortByAgi() // 排序並顯示所有角色與怪物的行動先後順序
{
	vector<string> names;
	vector<char> icons;
	vector<int> agisFirst;
	vector<int> agisSecond; // 輔助用，monster 皆為 0
	vector<Character> c = this->getCharacterJoin();
	vector<Monster> m = this->getMonsterOnMap();
	for (int i = 0; i < c.size(); i++)
	{
		names.push_back(c[i].getName());
		icons.push_back(c[i].getIcon());
		agisFirst.push_back(c[i].getAgiFirst());
		agisSecond.push_back(c[i].getAgiSecond());
	}
	for (int i = 0; i < m.size(); i++)
	{
		names.push_back(m[i].getName());
		icons.push_back(m[i].getIcon());
		agisFirst.push_back(m[i].getAgi());
		agisSecond.push_back(0);
	}

	if (names.size() != 0)
	{
		for (int i = 0; i < names.size() - 1; i++)
		{
			for (int j = i; j < names.size(); j++)
			{
				if (agisFirst[i] > agisFirst[j]) // 如果 i 的 agi > j 的 agi
				{
					string tempN = names[i];
					char tempI = icons[i];
					int tempF = agisFirst[i];
					int tempS = agisSecond[i];

					names[i] = names[j];
					icons[i] = icons[j];
					agisFirst[i] = agisFirst[j];
					agisSecond[i] = agisSecond[j];

					names[j] = tempN;
					icons[j] = tempI;
					agisFirst[j] = tempF;
					agisSecond[j] = tempS;

				}

				if (agisFirst[i] == agisFirst[j]) // 如果 i 的 agi == j 的 agi
				{
					if (islower(icons[i]) && isupper(icons[j])) // 如果 i 是怪物 (islower()) 且 j 是角色(isupper())
					{
						string tempN = names[i];
						char tempI = icons[i];
						int tempF = agisFirst[i];
						int tempS = agisSecond[i];

						names[i] = names[j];
						icons[i] = icons[j];
						agisFirst[i] = agisFirst[j];
						agisSecond[i] = agisSecond[j];

						names[j] = tempN;
						icons[j] = tempI;
						agisFirst[j] = tempF;
						agisSecond[j] = tempS;
					}

					if (isupper(icons[i]) && isupper(icons[j])) // 如果 i 是角色(isupper()) 且 j 是角色(isupper())
					{
						if (agisSecond[i] > agisSecond[j]) // 如果 i 的第二個 agi > j 的第二個 agi
						{
							string tempN = names[i];
							char tempI = icons[i];
							int tempF = agisFirst[i];
							int tempS = agisSecond[i];

							names[i] = names[j];
							icons[i] = icons[j];
							agisFirst[i] = agisFirst[j];
							agisSecond[i] = agisSecond[j];

							names[j] = tempN;
							icons[j] = tempI;
							agisFirst[j] = tempF;
							agisSecond[j] = tempS;
						}
						if (agisSecond[i] == agisSecond[j]) // 如果 i 的第二個 agi == j 的第二個 agi
						{
							if (icons[i] > icons[j]) // 如果 i 的 icon > j 的 icon
							{
								string tempN = names[i];
								char tempI = icons[i];
								int tempF = agisFirst[i];
								int tempS = agisSecond[i];

								names[i] = names[j];
								icons[i] = icons[j];
								agisFirst[i] = agisFirst[j];
								agisSecond[i] = agisSecond[j];

								names[j] = tempN;
								icons[j] = tempI;
								agisFirst[j] = tempF;
								agisSecond[j] = tempS;
							}

						}
					}
					
					if (islower(icons[i]) && islower(icons[j])) //如果 i 是怪物(islower()) 且 j 是怪物(islower())
					{
						if (names[i] > names[j]) // 如果 i 的名字 > j 的名字
						{
							string tempN = names[i];
							char tempI = icons[i];
							int tempF = agisFirst[i];
							int tempS = agisSecond[i];

							names[i] = names[j];
							icons[i] = icons[j];
							agisFirst[i] = agisFirst[j];
							agisSecond[i] = agisSecond[j];

							names[j] = tempN;
							icons[j] = tempI;
							agisFirst[j] = tempF;
							agisSecond[j] = tempS;
						}

						if (names[i] == names[j]) // 如果 i 的名字 == j 的名字
						{
							if (icons[i] > icons[j]) // 如果 i 的 icon > j 的 icon
							{
								string tempN = names[i];
								char tempI = icons[i];
								int tempF = agisFirst[i];
								int tempS = agisSecond[i];

								names[i] = names[j];
								icons[i] = icons[j];
								agisFirst[i] = agisFirst[j];
								agisSecond[i] = agisSecond[j];

								names[j] = tempN;
								icons[j] = tempI;
								agisFirst[j] = tempF;
								agisSecond[j] = tempS;
							}
						}
					}

				}

			}
		}
	}

	this->sortedIcons.clear();
	this->sortedIcons = icons;


	//for (int i = 0; i < sortedIcons.size(); i++)
	//{
	//	cout << "agi: " << agisFirst[i] << "  names: " << names[i] 
	//		<< "  icons: " << icons[i] << endl;
	//}
}

void Game::showTurn()
{
	vector<string> nameList;// 記錄印過什麼怪物
	for (int i = 0; i < sortedIcons.size(); i++)
	{
		int index = 0;
		if (islower(sortedIcons[i])) // 如果是怪獸，有可能還沒出現
		{
			index = findMonsterPos(sortedIcons[i], monsterOnMap);
			Monster* mPtr = &monsterOnMap[index];

			// 如果已經印過，跳過
			bool skip = false;
			for (int i = 0; i < nameList.size(); i++)
			{
				if (nameList[i] == mPtr->getName())
				{
					skip = true;
				}
			}
			if (skip) continue;

			// 如果沒印過
			Point2d position = mPtr->getPosition();
			bool visible = map.getVisibility(position);
			if (visible)
			{
				cout << mPtr->getName() << " " <<
					integerToString(mPtr->getAgi()) << " ";

				vector<MonsterSkill> h = mPtr->getHand();
				int id = mPtr->getSelectedCardId();
				vector<string> skills = h[id].getSkills();
				for (int j = 0; j < skills.size(); j++)
				{
					cout << skills[j] << " ";
				}
				cout << "\n";

				// 印過的加到 nameList 中
				nameList.push_back(mPtr->getName());
			}
		}
		else
		{
			index = findCharacterPos(sortedIcons[i], characterJoin);
			Character* cPtr = &characterJoin[index];
			cout << cPtr->getIcon() << " " << integerToString(cPtr->getAgiFirst())
				<< " ";
			vector<int> cardId = cPtr->getSelectedCardId();
			for (int j = 0; j < cardId.size(); j++)
			{
				cout << cardId[j];
				if (j != cardId.size() - 1)
					cout <<  " ";
			}
			cout << "\n";
		}
	}
	nameList.clear();
}

void Game::battle() // 依照 agi 戰鬥，如果怪物還沒出現在地圖上則跳過，但卡牌一樣紀錄已使用
{
	clearShield(); // 每輪開始前先清除角色和怪物之前的護甲值

	for (int i = 0; i < sortedIcons.size(); i++)
	{
		// 死掉的都不用動作
		// 為避免 sortedIcons 的對照亂掉，刪除角色或怪物統一等 battle 後處理
		// 這裡如果死掉，都只標記角色或怪物已經死亡
		int index = 0;
		if (islower(sortedIcons[i])) // 如果是怪獸，有可能還沒出現
		{
			index = findMonsterPos(sortedIcons[i], monsterOnMap);
			Monster* mPtr = &monsterOnMap[index];
			Point2d position = mPtr->getPosition();
			bool visible = map.getVisibility(position);

			if (mPtr->isAlive() && visible) // 如果怪物還沒死 + 已經出現在地圖上
			{
				act(monsterOnMap[index]); // 怪物執行動作

				// 以種類為單位，怪獸此輪的卡都被記為已用過 
				vector<MonsterSkill> h = mPtr->getHand();
				int id = mPtr->getSelectedCardId();
				mPtr->useCard(id);

				// 如果遇到可以重複使用的牌，牌回收
				if (h[id].getReusable())
					mPtr->resetHandStatus();
			}
		}
		else // 如果是角色
		{
			index = findCharacterPos(sortedIcons[i], characterJoin);
			Character* cPtr = &characterJoin[index];

			if (!cPtr->isAlive()) // 如果角色已經陣亡
				continue; // 這輪不執行動作

			cout << cPtr->getIcon() << "'s turn: card ";
			vector<int> cardId = cPtr->getSelectedCardId();



			if (cardId.size() == 1) // 長休
			{
				cout << "-1\n";
				// 輸入要刪除哪一張 (需檢查輸入是否合法，先假設輸入一定合法)
				string idStr;
				int id = 0;
				bool notVaild = true;
				while (notVaild)
				{
					getline(cin, idStr);

					if (idStr.find("check") != string::npos)
						cPtr->check();
					else
					{
						bool allDigit = true;
						for (int i = 0; i < idStr.length(); i++)
						{
							if (!isdigit(idStr[i]))
							{
								allDigit = false;
								break;
							}

						}
						if (!allDigit)
						{
							cout << "error: please enter the card you want to discard\n";
							continue;
						}

						id = stringToInteger(idStr);
						// 確認牌存在
						int pos = cPtr->findCardPos(id);
						if (pos == -1)
						{
							cout << "error: the card id does not exist\n";
							continue;
						}
						if (cPtr->getHandStatus()[pos] == 1)
						{
							cout << "error: cannot discard this card\n";
							continue;
						}

						notVaild = false;
					}
				}
			
				// 刪除 + 回血 + 將棄牌堆移回手牌
				cPtr->delCard(id);
				int value = 2;
				heal(*cPtr, value); // 這樣應該是丟 reference 吧
				cPtr->resetHandStatus();
				
				// 輸出訊息到螢幕上
				cout << cPtr->getIcon() << " heal " << value << ", now hp is "
					<< cPtr->getHp() << endl;
				cout << "remove card: " << id << endl;
			}

			else
			{
				for (int j = 0; j < cardId.size(); j++) // 輸出本輪使用的牌
				{
					
					cout << cardId[j];
					if (j != cardId.size() - 1)
						cout << " ";
				}
				cout << "\n";

				string order; // 記錄第一張執行的牌和上下
				// 讀 check 或要執行的第一個動作 (輸入直到合法)
				bool notVaild = true;
				while (notVaild)
				{
					getline(cin, order);
					if (order == "check")
					{
						// 如果 check 
							// 如果角色還活著 + 已經出現在地圖上
								// 輸出血量
							// 如果怪物還活著 + 已出現在地圖上
						check();
					}
					else
					{
						// 要檢查選擇的牌是合理的，不然程式超容易壞掉
						if (order.length() != 2)
						{
							cout << "error: please enter check or card id + u/d\n";
							continue;
						}
						if (order[1] != 'u' && order[1] != 'd')
						{
							cout << "error: please enter check or card id + u/d\n";
							continue;
						}
						
						int id = stringToInteger(order.substr(0, 1));
						if (id != characterJoin[index].getSelectedCardId()[0] && id != characterJoin[index].getSelectedCardId()[1])
						{
							cout << "error: you do not select card " << id << "\n";
							continue;
						}
				
						notVaild = false;
					}
				}

				act(characterJoin[index], order);

				// 記為已用過
				for (int j = 0; j < cardId.size(); j++)
				{
					cPtr->useCard(cardId[j]);
				}
			}
		}
	}

	// 每輪結束開門 (若有打開門則更新地圖, 反之不用)
	openDoor();
}

void Game::check() // 檢查所有存活單位的現在血量與本回合護甲值
{
	// 顯示的順序依照字母序由小到大顯示(先角色再怪物)
	for (int i = 0; i < characterJoin.size(); i++)
	{
		cout << characterJoin[i].getIcon() << "-hp: " << characterJoin[i].getHp()
			<< ", shield: " << characterJoin[i].getShield() << endl;
	}

	// 已死亡單位和未出現怪物(未顯示區域的)不會顯示; 
	for (int i = 0; i < monsterOnMap.size(); i++)
	{
		Point2d pos = monsterOnMap[i].getPosition();
		if (this->map.getVisibility()[pos.getX()][pos.getY()])
		{
			cout << monsterOnMap[i].getIcon() << "-hp: " << monsterOnMap[i].getHp()
				<< ", shield: " << monsterOnMap[i].getShield() << endl;
		}
	}
}

void Game::updateCharacterJoin() // 刪除已死亡和不能出牌&長休的角色，並更新地圖
{
	// 有兩種狀況
	// 上一輪已經被殺死，不用輸出 (未測試++++++++++++)
	vector<Character> temp;
	for (int i = 0; i < characterJoin.size(); i++)
	{
		if (characterJoin[i].isAlive())
		{
			temp.push_back(characterJoin[i]);
		}
	}
	
	characterJoin.clear();
	characterJoin = temp;

	temp.clear();
	bool killing = false;
	// 沒有牌可出也不能長休，殺死，輸出訊息
	for (int i = 0; i < characterJoin.size(); i++)
	{
		if (characterJoin[i].canPlay() || characterJoin[i].canRest())
		{
			temp.push_back(characterJoin[i]);
		}

		else
		{
			cout << characterJoin[i].getIcon() << " is killed!!\n";
			this->map.erase(characterJoin[i].getPosition());
			killing = true;
			characterJoinCnt -= 1;
		}
	}

	if (killing)
		this->map.display();

	characterJoin.clear();
	characterJoin = temp;
}

void Game::updateAllEntity() // 刪除戰鬥後死亡的角色和怪物 
{
	// 先刪角色
	vector<Character> tempC;
	for (int i = 0; i < characterJoin.size(); i++)
	{
		if (characterJoin[i].isAlive())
		{
			tempC.push_back(characterJoin[i]);
		}
		else
		{
			characterJoinCnt -= 1;
		}
	}

	characterJoin.clear();
	characterJoin = tempC;
	tempC.clear();

	// 後刪怪物
	vector<Monster> tempM;
	for (int i = 0; i < monsterOnMap.size(); i++)
	{
		if (monsterOnMap[i].isAlive())
		{
			tempM.push_back(monsterOnMap[i]);
		}
	}

	monsterOnMap.clear();
	monsterOnMap = tempM;
	tempM.clear();
}

void Game::act(Monster& m)
{
	vector<MonsterSkill> h = m.getHand();
	int id = m.getSelectedCardId();
	vector<string> action = h[id].getSkills(); // 取得卡片的技能敘述
	for (int i = 0; i < action.size(); i++) // 從第一個敘述到最後一個敘述
	{
		// 怪物的技能敘述攻擊和移動和射程數值皆為調整值
		// 近戰怪物不會有遠程攻擊，遠程怪物不會有近戰攻擊

		if (action[i].find("range") != string::npos) // 遠距攻擊
		{
			size_t posR = action[i].find("range");
			int rangeAdj = stringToInteger(action[i].substr(posR + 6)); // 調整值

			// attack 一律找目標 -> 目標受傷害
			char targetIcon = findTarget(m, m.getRange() + rangeAdj);
			if (targetIcon == '0')
			{
				cout << "no one lock\n";
			}
			else
			{
				size_t posV = action[i].find(" "); // 第一個空白的位置
				int attackV = m.getDamage() + stringToInteger(action[i].substr(posV + 1, posR - posV - 2));
				Character& t = findCharacter(targetIcon, characterJoin);
				
				// 輸出鎖定的目標
				int distance = this->map.countDistance(m.getPosition(), t.getPosition());
				cout << m.getIcon() << " lock " << t.getIcon() << " in distance " << distance << "\n";

				// 被攻擊的人扣掉血量
				t.isAttacked(attackV - t.getShield());
				cout << m.getIcon() << " attack " << t.getIcon() << " " << attackV << " damage, "
					<< t.getIcon() << " shield " << t.getShield() << ", "
					<< t.getIcon() << " remain " << t.getHp() << " hp\n";
				if (debugMode == 0)
				{
					string pause;
					getline(cin, pause);
				}

				// 檢查被攻擊對象是否死亡
				if (t.getHp() <= 0)
				{
					t.isKilled();
					cout << t.getIcon() << " is killed!!\n";
					// 將死亡對象從地圖上清除
					this->map.erase(t.getPosition());
					// 輸出更新後的地圖
					this->map.display();
					if (debugMode == 0)
					{
						string pause;
						getline(cin, pause);
					}
				}
			}
		}
		else if (action[i].find("attack") != string::npos) // 近距攻擊，可想成 range = 1
		{
			// attack 一律找目標 -> 目標受傷害
			char targetIcon = findTarget(m, 1);

			if (targetIcon == '0')
			{
				cout << "no one lock\n";
			}
			else
			{
				size_t posV = action[i].find(" ");
				int attackV = m.getDamage() + stringToInteger(action[i].substr(posV + 1));
				Character& t = findCharacter(targetIcon, characterJoin);

				// 被攻擊的人扣掉血量
				t.isAttacked(attackV - t.getShield());
				cout << m.getIcon() << " attack " << t.getIcon() << " " << attackV << " damage, "
					<< t.getIcon() << " shield " << t.getShield() << ", "
					<< t.getIcon() << " remain " << t.getHp() << " hp\n";
				if (debugMode == 0) 
				{
					string pause;
					getline(cin, pause);
				}
				// 檢查被攻擊對象是否死亡
				if (t.getHp() <= 0)
				{
					t.isKilled();
					cout << t.getIcon() << " is killed!!\n";
					// 將死亡對象從地圖上清除
					this->map.erase(t.getPosition());
					// 輸出更新後的地圖
					this->map.display();
					if (debugMode == 0)
					{
						string pause;
						getline(cin, pause);
					}
				}

			}
		}
		else if (action[i].find("move") != string::npos)
		{
			size_t pos = action[i].find(" ");
			string direction = action[i].substr(pos + 1);
			move(m, direction);
			if (debugMode == 0)
			{
				string pause;
				getline(cin, pause);
			}
		}
		else if (action[i].find("shield") != string::npos)
		{
			size_t pos = action[i].find(" ");
			string valueStr = action[i].substr(pos + 1);
			int value = stringToInteger(valueStr);
			shield(m, value);
			cout << m.getIcon() << " shield " << value << " this turn\n";
			if (debugMode == 0)
			{
				string pause;
				getline(cin, pause);
			}
		}
		else if (action[i].find("heal") != string::npos)
		{
			size_t pos = action[i].find(" ");
			string valueStr = action[i].substr(pos + 1);
			int value = stringToInteger(valueStr);
			heal(m, value);
			cout << m.getIcon() << " heal " << value << ", now hp is " << m.getHp() << endl;
			if (debugMode == 0)
			{
				string pause;
				getline(cin, pause);
			}
		}
	}

}

void Game::act(Character& c, string order)
{
	// 把第一個執行的動作拆成數字(int) + 上下(char)
	vector<int> cardId = c.getSelectedCardId();
	char firstPart = order[order.length() - 1]; // u or d
	string numStr = order.substr(0, order.length() - 1);
	int first = stringToInteger(numStr); // 除了最後一個字母不要
	
	// 把第二個執行的動作拆成數字(int) + 上下(char)
	char secondPart;
	if (firstPart == 'u') secondPart = 'd';
	else secondPart = 'u';
	int second = 0;
	for (int i = 0; i < cardId.size(); i++)
	{
		if (cardId[i] != first)
		{
			second = cardId[i];
			break;
		}
	}

	for (int i = 0; i < 2; i++) // 執行兩個動作
	{
		vector<string> action; // 動作敘述
		if (i == 0)
		{
			int pos = c.findCardPos(first);
			CharacterSkill card = c.getHand()[pos]; // 手牌的位置 != 卡片編號
			if (firstPart == 'u') action = card.getFirstSkills();
			else action = card.getSecondSkills();
		}
		else
		{
			int pos = c.findCardPos(second);
			CharacterSkill card = c.getHand()[pos]; 
			if (secondPart == 'u') action = card.getFirstSkills();
			else action = card.getSecondSkills();
		}

		for (int i = 0; i < action.size(); i++)
		{
			if (action[i].find("range") != string::npos) // 遠距攻擊
			{
				size_t posR = action[i].find("range");

				int range = stringToInteger(action[i].substr(posR + 6));
				// attack 一律找目標 -> 目標受傷害
				char targetIcon = findTarget(c, range);

				if (targetIcon != '0')
				{
					size_t posV = action[i].find(" "); // 找第一個空白的位置
					int attackV = stringToInteger(action[i].substr(posV + 1, posR - posV - 2)); // attack 3 range 2
					Monster& t = findMonster(targetIcon, monsterOnMap);

					// 被攻擊的人扣掉血量
					t.isAttacked(attackV - t.getShield());
					cout << c.getIcon() << " attack " << t.getIcon() << " " << attackV << " damage, "
						<< t.getIcon() << " shield " << t.getShield() << ", "
						<< t.getIcon() << " remain " << t.getHp() << " hp\n";

					// 檢查被攻擊對象是否死亡
					if (t.getHp() <= 0)
					{
						t.isKilled();
						cout << t.getIcon() << " is killed!!\n";
						// 將死亡對象從地圖上清除
						this->map.erase(t.getPosition());
						// 輸出更新後的地圖
						this->map.display();
					}
				}

			}
			else if (action[i].find("attack") != string::npos) // 近距攻擊，可想成 range = 1
			{
				char targetIcon = findTarget(c, 1);
				
				if (targetIcon != '0')
				{
					size_t posV = action[i].find(" ");
					int attackV = stringToInteger(action[i].substr(posV + 1));
					Monster& t = findMonster(targetIcon, monsterOnMap);

					// 被攻擊的人扣掉血量
					t.isAttacked(attackV - t.getShield());
					cout << c.getIcon() << " attack " << t.getIcon() << " " << attackV << " damage, "
						<< t.getIcon() << " shield " << t.getShield() << ", "
						<< t.getIcon() << " remain " << t.getHp() << " hp\n";

					// 檢查被攻擊對象是否死亡
					if (t.getHp() <= 0)
					{
						t.isKilled();
						cout << t.getIcon() << " is killed!!\n";
						// 將死亡對象從地圖上清除
						this->map.erase(t.getPosition());
						// 輸出更新後的地圖
						this->map.display();
					}
				}
			}
			else if (action[i].find("move") != string::npos)
			{
				size_t pos = action[i].find(" ");
				int maxStep = stringToInteger(action[i].substr(pos + 1));
				move(c, maxStep);
			}
			else if (action[i].find("shield") != string::npos)
			{
				size_t pos = action[i].find(" ");
				string valueStr = action[i].substr(pos + 1);
				int value = stringToInteger(valueStr);
				shield(c, value);
				cout << c.getIcon() << " shield " << value << " this turn\n";
			}
			else if (action[i].find("heal") != string::npos)
			{
				size_t pos = action[i].find(" ");
				string valueStr = action[i].substr(pos + 1);
				int value = stringToInteger(valueStr);
				heal(c, value);
				cout << c.getIcon() << " heal " << value << ", now hp is " << c.getHp() << endl;
			}
		}

	}
}

void Game::move(Monster& m, string str) // 怪物移動 str
{
	Point2d p = m.getPosition();//一樣先存取一個先前得位置好去更新與修改;
	Map map = getMap();
	vector<Point2d> everyStep;
	this->map.erase(m.getPosition()); // 先清除在地圖上的位置
	for (int i = 0; i < str.size(); i++) {
		if (str[i] == 'e') {
			break;
		}
		else if (str[i] == 'w') {
			p.setX(p.getX() - 1);
			everyStep.push_back(p);
		}
		else if (str[i] == 's') {
			p.setX(p.getX() + 1);
			everyStep.push_back(p);
		}
		else if (str[i] == 'a') {
			p.setY(p.getY() - 1);
			everyStep.push_back(p);
		}
		else if (str[i] == 'd') {
			p.setY(p.getY() + 1);
			everyStep.push_back(p);
		}
		//cout << "Position : " << p.getX() << " " << p.getY() << endl;
	}
	this->map.draw(m.getPosition(), map.getDefaultMap()[m.getPosition().getX()][m.getPosition().getY()]);
	if (str != "e") decideFinalPosition(m, everyStep);
	this->map.draw(m.getPosition(), m.getIcon()); // 把新位置畫在地圖上
	this->map.display();
	//cout << m.getIcon() << " try to move\n"; // 測試用
}

void Game::decideFinalPosition(Monster& m, vector<Point2d> historyMove) {
	vector<char> historyicon;
	Point2d p = m.getPosition();
	int finalidx = -1;
	for (int i = 0; i < historyMove.size(); i++) {
		int x = historyMove[i].getX();
		int y = historyMove[i].getY();
		//cout << endl << this->map.getMap()[x][y] << endl;
		historyicon.push_back(this->map.getMap()[x][y]);
		//去記錄每一步都是遇到什麼地圖icon
	}
	for (int i = 0; i < historyicon.size(); i++) {
		if (historyicon[i] == '1') finalidx = i;
		//如果移動時icon為1，代表移動合法。最終位置的idx為i
		else if (historyicon[i] == '2' || historyicon[i] == '3' || isupper(historyicon[i]) ||
			historyMove[i].getX() < 0 || historyMove[i].getY() < 0 ||
			historyMove[i].getX() > this->map.getColCnt() - 1 || historyMove[i].getY() > this->map.getRowCnt() - 1 ||
			historyicon[i] == ' ') {
			break;
			//如果遇到角色、門、障礙物 或是 超過了地圖邊界(大於column,row、小於0)
			//直接跳出迴圈
		}
		else if (islower(historyicon[i])) {
			continue;
			//若是遇到怪物，合法通過但是不更新finalidx因為沒辦法停下來。繼續下一步
		}
	}
	if (finalidx == -1) {
		m.setPosition(p);
		//若是idx還在-1代表原地不動
	}
	else {
		p = historyMove[finalidx];
		m.setPosition(p);
		//若是idx不等於-1，代表被更新過
		//更新怪物最新位置
	}
}

void Game::shield(Monster& m, int value) // 怪物獲得護甲值 value
{
	m.setShield(value);
}

void Game::heal(Monster& m, int value) // 怪物 hp 恢復 value
{
	MonsterData monster = findMonsterData(m.getName(), this->allMonsterData);
	string level = m.getLevel();
	int maxHp = 0;
	if (level == "base") maxHp = monster.getBaseHp();
	if (level == "elite") maxHp = monster.getEliteHp();
	if (m.getHp() + value > maxHp)
	{
		m.setHp(maxHp);
	}
	else m.setHp(m.getHp() + value);
}

char Game::findTarget(Monster& m, int range) // 怪物找攻擊目標，回傳要攻擊的角色的 icon，不攻擊回傳 '0'
{
	//cout << m.getIcon() << " try to find target and attack\n"; // 測試用
	bool find = false;
	Map map = getMap();
	char target = '0';
	vector<char> allCharacterIcon = getSortedIcons();
	vector<Character> allC = getCharacterJoin();
	for (int i = 0; i < allCharacterIcon.size(); i++) {
		for (int j = 0; j < allC.size(); j++) {
			if (allCharacterIcon[i] == allC[j].getIcon()) {
				if (map.canSee(allC[j].getPosition(), m.getPosition()) &&
					range >= map.countDistance(allC[j].getPosition(), m.getPosition())) {
					target = allCharacterIcon[i];
					find = true;
					break;
				}
			}
		}
		if (find) break;
	}
	return target;
}

void Game::move(Character& c, int maxStep) // 角色移動，最多 maxStep 步 ++++++++++++++++++未測試
{
	this->map.erase(c.getPosition()); // 先清除在原本角色在地圖上的位置
	string command;
	vector<Point2d> everyStep;
	bool Error = true;
	while (Error) {
		cin >> command;
		cin.ignore();
		if (command == "check")
		{
			// 如果 check 
				// 如果角色還活著 + 已經出現在地圖上
					// 輸出血量
				// 如果怪物還活著 + 已出現在地圖上
			check();
			continue;
		}
		if (command == "e") break;
		bool notVaild = false;
		for (int i = 0; i < command.length(); i++)
		{
			if (command[i] != 'w' && command[i] != 'a' && command[i] != 's' && command[i] != 'd')
			{
				notVaild = true;
			}
		}
		if (notVaild)
		{
			cout << "error: please enter e or w/a/s/d\n";
			continue;
		}
		if (command.size() > maxStep) {
			Error = true;
		}
		else {
			Point2d p = c.getPosition();
			for (int i = 0; i < command.size(); i++) {
				if (command[i] == 'e') {
					Error = false;
					break;
				}
				else if (command[i] == 'w') {
					p.setX(p.getX() - 1);
					everyStep.push_back(p);
				}
				else if (command[i] == 's') {
					p.setX(p.getX() + 1);
					everyStep.push_back(p);
				}
				else if (command[i] == 'a') {
					p.setY(p.getY() - 1);
					everyStep.push_back(p);
				}
				else if (command[i] == 'd') {
					p.setY(p.getY() + 1);
					everyStep.push_back(p);
				}
			}
			if (command != "e") Error = decideFinalPosition(c, everyStep);
		}
		if (Error == true) { std::cout << "error move!!!\n"; }
		command.clear();
		everyStep.clear();
	}

	//this->map.erase(c.getPosition()); // 先清除在原本角色在地圖上的位置
	//string command;
	//vector<Point2d> everyStep;
	//bool error = true;
	//while (error)
	//{
	//	bool inputing = true;
	//	while (inputing) {
	//		getline(cin, command);

	//		if (command.size() > maxStep) {
	//			cout << "error move!!!";
	//			//是否超過最大步數
	//			continue;
	//		}

	//		bool vaild = true; // 檢查使用者輸入是否合法
	//		for (int i = 0; i < command.size(); i++)
	//		{
	//			if (command[i] != 'e' && command[i] != 'w' && command[i] != 'a'
	//				&& command[i] != 's' && command[i] != 'd')
	//			{
	//				cout << "error: invaild command\n";
	//				vaild = false;
	//				break;
	//			}

	//			if (command[i] == 'e' && command.size() != 1)
	//			{
	//				cout << "error: invaild command\n";
	//				vaild = false;
	//				break;
	//			}
	//		}

	//		if (!vaild) continue;

	//		inputing = false;
	//		for (int i = 0; i < command.size(); i++) {
	//			Point2d p = c.getPosition();
	//			if (command[i] == 'e') {
	//				break;
	//			}
	//			else if (command[i] == 'w') {
	//				p.setY(p.getY() - 1);
	//				everyStep.push_back(p);
	//			}
	//			else if (command[i] == 's') {
	//				p.setY(p.getY() + 1);
	//				everyStep.push_back(p);
	//			}
	//			else if (command[i] == 'a') {
	//				p.setX(p.getX() - 1);
	//				everyStep.push_back(p);
	//			}
	//			else if (command[i] == 'd') {
	//				p.setX(p.getX() + 1);
	//				everyStep.push_back(p);
	//			}
	//		}
	//	}
	//}

	//if (command != "e") decideFinalPosition(c, everyStep);
	//map.draw(c.getPosition(), c.getIcon());
	//map.display();
	map.draw(c.getPosition(), c.getIcon());
	map.display();
}

bool Game::decideFinalPosition(Character& c, vector<Point2d> historyMove) {
	Map map = getMap();
	vector<char> historyicon;
	Point2d p = c.getPosition();
	int finalidx = -1;
	bool MoveInvalid = true;
	for (int i = 0; i < historyMove.size(); i++) {
		int x = historyMove[i].getX();
		int y = historyMove[i].getY();
		historyicon.push_back(map.getMap()[x][y]);
		//去記錄每一步都是遇到什麼地圖icon
	}
	for (int i = 0; i < historyicon.size(); i++) {
		if (historyicon[i] == '1' || historyicon[i] == '3') { finalidx = i; }
		//如果移動時icon為1，代表移動合法。最終位置的idx為i
		else if (historyicon[i] == '2' || islower(historyicon[i]) || !map.isOpen(historyMove[i]) ||
			historyMove[i].getX() < 0 || historyMove[i].getY() < 0 ||
			historyMove[i].getX() > map.getRowCnt() - 1 || historyMove[i].getY() > map.getColCnt() - 1 || historyicon[i]==' ') {
			return true;
			//如果遇到怪物、"""門"""、障礙物 或是 超過了地圖邊界(大於column,row、小於0)
			//直接跳出迴圈
		}
		else if (isupper(historyicon[i]) && i != historyicon.size()-1) {
			continue;
			//若是遇到角色，合法通過但是不更新finalidx因為沒辦法停下來。繼續下一步
		}
		else if (isupper(historyicon[i]) && i == historyicon.size()-1) return true;
	}
	p = historyMove[finalidx];
	if (isupper(historyicon[finalidx])) return true;
	c.setPosition(p);
	MoveInvalid = false;
	return MoveInvalid;
	//更新怪物最新位置
}

void Game::shield(Character& c, int value) // 角色獲得護甲值 value
{
	c.setShield(value);
}

void Game::heal(Character& c, int value) // 角色回血 value
{
	CharacterData character = findCharacterData(c.getName(), this->allCharacterData);
	if (c.getHp() + value > character.getHp()) {
		c.setHp(character.getHp());
	}
	else { c.setHp(c.getHp() + value); }
}

char Game::findTarget(Character& c, int range) // 角色找攻擊目標，回傳要攻擊的怪物的 icon，不攻擊回傳 '0'
{
	//cout << c.getIcon() << " try to find target and attack\n"; // 測試用
	char target = '0';
	bool invalid = true;
	vector<Monster> M = getMonsterOnMap();
	Map map = getMap();
	//for (int i = 0; i < M.size(); i++) {
	//	cout << "Monster : " << M[i].getIcon() << " is on the map. \n";
	//}
	while (invalid) {
		//cout << "Please enter the target.\n";
		string str;
		getline(cin, str);
		if (str == "check")
		{
			// 如果 check 
				// 如果角色還活著 + 已經出現在地圖上
					// 輸出血量
				// 如果怪物還活著 + 已出現在地圖上
			check();
			continue;
		}
		if (str.length() != 1 || (!islower(str[0]) && str[0] != '0'))
		{
			//cout << "Please enter the target.\n";
			continue;
		}
		target = str[0];
		if (target == '0') {
			invalid = false;
			break;
		}
		else {
			for (int i = 0; i < M.size(); i++) {
				if (target == M[i].getIcon()) {
					if (range >= map.countDistance(c.getPosition(), M[i].getPosition()) &&
						map.canSee(c.getPosition(), M[i].getPosition())) {
						invalid = false;
						break;
					}
					//else {
					//	cout << "Cannot See The Target Or The Move Is Out The Range.\n";
					//}
				}
				if (invalid == false) break;
			}
		}
		//if (isupper(target)) cout << "You cannot attack your team.\n";
		if (invalid == true) {
			cout << "error target!!!\n";
		}
	}
	//if (target != '0') {
	//	cout << "find target " << target << endl;
	//	cout << "\n attack it.\n";
	//}
	//else cout << "no is one on lock.\n";
	return target;
}

void Game::clearShield() // 每輪開始前先清除角色和怪物之前的護甲值
{
	// 先清角色
	for (int i = 0; i < characterJoin.size(); i++)
		characterJoin[i].setShield(0);

	// 後清怪物
	for (int i = 0; i < monsterOnMap.size(); i++)
		monsterOnMap[i].setShield(0);
}

void Game::openDoor() // 每輪結束開門 (若有打開門則更新地圖, 反之不用)
{
	int visibleMonsterCnt = 0; // 計算可見區域內還存活的怪物
	for (int i = 0; i < monsterOnMap.size(); i++)
	{
		Point2d pos = monsterOnMap[i].getPosition();
		int x = pos.getX();
		int y = pos.getY();
		if (this->map.getVisibility()[x][y] && monsterOnMap[i].isAlive())
			visibleMonsterCnt += 1;
	}

	if (visibleMonsterCnt == 0) // 如果可見區域內的怪物都被清光光
	{
		bool openArea = false;
		// 可以一次開啟多個區域
		for (int i = 0; i < characterJoin.size(); i++)
		{
			Point2d pos = characterJoin[i].getPosition();
			int x = pos.getX();
			int y = pos.getY();

			if (this->map.getDefaultMap()[x][y] == '3')  // 可以開啟區域
			{
				Point2d open;
				// 上
				if (this->map.getDefaultMap()[x - 1][y] == '1' && this->map.getVisibility()[x - 1][y] == false)
					open = Point2d(x - 1, y);
				// 下
				else if (this->map.getDefaultMap()[x + 1][y] == '1' && this->map.getVisibility()[x + 1][y] == false)
					open = Point2d(x + 1, y);
				// 左
				else if (this->map.getDefaultMap()[x][y - 1] == '1' && this->map.getVisibility()[x][y - 1] == false)
					open = Point2d(x, y - 1);
				// 右
				else if (this->map.getDefaultMap()[x][y + 1] == '1' && this->map.getVisibility()[x][y + 1] == false)
					open = Point2d(x, y + 1);

				this->map.openArea(open, pos);
				openArea = true;
			}
		}

		if (openArea) this->map.display(); // 開完門之後，顯示更新後的地圖
	}
}

void Game::showWinner() // 螢幕上印出角色或怪物獲勝
{
	if (characterWin)
		cout << "character win~" << endl;
	else
		cout << "monster win~" << endl;
}

int findCharacterDataPos(string name, vector<CharacterData> allCharacterData) // 從名字判斷角色是否存在
{ 
	int pos = -1; // 存在回傳位置，不存在回傳 -1
	for (int i = 0; i < allCharacterData.size(); i++)
	{
		if (allCharacterData[i].getName() == name)
		{
			pos = i; 
			break;
		}
	}
	return pos;
}

int findCharacterPos(char icon, vector<Character> c)
{
	int pos = -1; // 存在回傳位置，不存在回傳 -1
	for (int i = 0; i < c.size(); i++)
	{
		if (c[i].getIcon() == icon)
		{
			pos = i;
			break;
		}
	}
	return pos;
}

int findMonsterPos(char icon, vector<Monster> m)
{
	int pos = -1; // 存在回傳位置，不存在回傳 -1
	for (int i = 0; i < m.size(); i++)
	{
		if (m[i].getIcon() == icon)
		{
			pos = i;
			break;
		}
	}
	return pos;
}

Character& findCharacter(char icon, vector<Character>& v)
{
	for (int i = 0; i < v.size(); i++)
	{
		if (v[i].getIcon() == icon)
			return v[i];
	}
}

Monster& findMonster(char icon, vector<Monster> &m)
{
	for (int i = 0; i < m.size(); i++)
	{
		if (m[i].getIcon() == icon)
			return m[i];
	}
}

CharacterData& findCharacterData(string name, vector<CharacterData>& allCharacterData)
{
	for (int i = 0; i < allCharacterData.size(); i++)
		if (allCharacterData[i].getName() == name)
			return allCharacterData[i];
}

MonsterData& findMonsterData(string name, vector<MonsterData>& allMonsterData)
{
	for (int i = 0; i < allMonsterData.size(); i++)
		if (allMonsterData[i].getName() == name)
			return allMonsterData[i];
}

void delPoint2d(vector<Point2d>& v, Point2d p) // 從 vector 刪除元素 
{
	vector<Point2d> temp;
	for (int i = 0; i < v.size(); i++)
	{
		if (v[i] != p)
			temp.push_back(v[i]);
	}
	v = temp;
}