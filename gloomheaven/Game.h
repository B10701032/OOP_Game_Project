#pragma once
#include <iostream>
#include <fstream>
#include <cstdlib> // 產生亂數
#include <ctime> // 亂數種子
#include <cctype>
#include <string>
#include <sstream>
#include <vector>
#include "CharacterData.h"
#include "CharacterSkill.h"
#include "Character.h"
#include "MonsterData.h"
#include "MonsterSkill.h"
#include "Monster.h"
#include "Map.h"
#include "HelperFunction.h"
using namespace std;
class Game
{
public:
	Game();
	Game(string characterFileName, string monsterFileName, bool debugMode);
	void startGame(); // 開啟一場新的遊戲
	void startGameTest(); // DEBUG 用
	void setAllCharacterData(); // 設定所有角色資料庫
	void setAllMonsterData(); // 設定所有怪物資料庫
	void setCharacterJoinCnt(); // 決定出場角色數量
	void setCharacterJoin(); // 決定出場角色與技能卡
	void setCharacterSkill(); // 決定每輪角色要打的牌
	void setCharacterPosition(); // 選擇角色起始位置
	void setMap(); // 決定地圖名和初始化地圖
	void setMonsterOnMap(); // 設定出場怪物資訊
	void setMonsterSkill_DEBUG(); // DEBUG_MODE = 1, 怪物的行動卡為依序出牌
	void setMonsterSkill(); // DEBUG_MODE = 0, 為所有已出現的怪物種類隨機抽一張行動卡
	void sortByAgi(); // 排序所有角色與怪物的行動先後順序
	void showTurn(); // 顯示所有角色與怪物的行動先後順序
	void setRoundCnt(int n) { this->roundCnt = n; } // 設定現在第幾回
	int getAllCharacterCnt() { return allCharacterCnt; }
	int getAllMonsterCnt() { return allMonsterCnt; }
	vector<CharacterData> getAllCharacterData() { return allCharacterData; }
	vector<MonsterData> getAllMonsterData() { return allMonsterData; }
	vector<Character> getCharacterJoin() { return characterJoin; }
	vector<Monster> getMonsterOnMap() { return monsterOnMap; }
	vector<char> getSortedIcons() { return sortedIcons; } // 取得所有角色與怪物的行動先後順序 (依照地圖上的記號
	Map getMap() { return map; }
	void battle(); // 依照 agi 戰鬥，如果怪物還沒出現在地圖上則跳過，但卡牌一樣紀錄已使用
	void check(); // 檢查所有存活單位的現在血量與本回合護甲值
	void updateCharacterJoin(); // 刪除不能出牌和長休的角色，並更新地圖
	void updateAllEntity(); // 刪除戰鬥後死亡的角色和怪物 
	void act(Monster& m); // 怪物執行動作
	void act(Character& c, string order); // 角色執行動作
	void move(Monster& m, string str); // 怪物移動 str
	void decideFinalPosition(Monster& m, vector<Point2d> historyMove); // 怪物移動用
	void shield(Monster& m, int value); // 怪物獲得護甲值 value
	void heal(Monster& m, int value); // 怪物回血 value
	char findTarget(Monster& m, int range); // 怪物找攻擊目標，回傳要攻擊的角色的 icon，不攻擊回傳 '0'
	void move(Character& c, int maxStep); // 角色移動，最多 maxStep 步
	bool decideFinalPosition(Character& c, vector<Point2d> historyMove); // 角色移動用
	void shield(Character& c, int value); // 角色獲得護甲值 value
	void heal(Character& c, int value); // 角色回血 value
	char findTarget(Character& c, int range); // 角色找攻擊目標，回傳要攻擊的怪物的 icon，不攻擊回傳 '0'
	void clearShield(); // 每輪開始前先清除角色和怪物之前的護甲值
	void openDoor(); // 每輪結束開門 (若有打開門則更新地圖, 反之不用)
	void showWinner(); // 螢幕上印出角色或怪物獲勝
private:
	string characterFileName; 
	string monsterFileName;
	bool debugMode;
	bool characterWin; // default 是 true
	int allCharacterCnt; // 資料庫裡的角色數量
	int allMonsterCnt; // 資料庫裡的怪物數量
	int characterJoinCnt; // 出場角色數量
	vector<CharacterData> allCharacterData; // 資料庫裡的角色
	vector<MonsterData> allMonsterData; // 資料庫裡的怪物
	vector<Character> characterJoin; // 出場的角色
	vector<Monster> monsterOnMap; // 出場的怪物
	vector<char> sortedIcons; // 依照地圖上的記號，記錄行動順序 (sort agi 用 
	Map map; // 遊戲地圖
	int roundCnt; // 記錄現在第幾回了
	bool battling; // 記錄戰鬥是否進行中
};

int findCharacterDataPos(string name, vector<CharacterData> allCharacterData); // 從名字判斷角色是否存在，存在回傳位置，不存在回傳 -1
int findCharacterPos(char icon, vector<Character> c);
int findMonsterPos(char icon, vector<Monster> m);
Character& findCharacter(char icon, vector<Character> &v); // 透過 icon 在 MonsterData 找某怪獸的資料
Monster& findMonster(char icon, vector<Monster> &m);
CharacterData& findCharacterData(string name, vector<CharacterData>& allCharacterData);
MonsterData& findMonsterData(string name, vector<MonsterData>& allMonsterData); // 透過名字在 MonsterData 找某怪獸的資料
void delPoint2d(vector<Point2d>& vector, Point2d p); // 從 vector<Point2d> 中刪除元素 
