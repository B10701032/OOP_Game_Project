#include "HelperFunction.h"

bool canOpenFile(string fileName) // 檢查檔案是否存在
{
	ifstream inStream;
	inStream.open(fileName);
	if (inStream.fail())
	{
		inStream.close();
		return false;
	}
	else
	{
		inStream.close();
		return true;
	}

}

bool palyAgainOrNot() // 決定是否開起遊戲
{
	string command;
	bool vaildCommand = false;
	while (!vaildCommand) // 若使用者不是輸入 play 或 exit，要求重新輸入
	{
		getline(cin, command);
		if (command == "play" || command == "exit")
			vaildCommand = true;
		else
			cout << "error command: please enter play or exit\n";
	}
	if (command == "play")
		return true;
	else
		return false;
}

int stringToInteger(string str) // 將 string 轉成 int
{
	bool isPositive = true; // 正或負
	if (str[0] == '-') isPositive = false;

	int pos = 0;
	for (int i = 0; i < str.length(); i++) // 刪除多餘的 0
	{
		if (str[i] != '-' && str[i] != '0')
		{
			pos = i;
			break;
		}
	}

	string numStr = str.substr(pos);

	int digit = numStr.length();

	int* numbers = new int[digit];
	for (int i = 0; i < digit; ++i)
		numbers[i] = numStr[i] - '0';

	int result = 0; 
	for (int i = 0; i < digit; i++)
		result += numbers[i] * pow(10, (digit - 1 - i));

	if (!isPositive && !(digit == 1 && numbers[0] == 0))
		result *= -1;

	delete[] numbers;

	return result;
}

string integerToString(int num) // 將 int 轉成 string
{
	string str = to_string(num);
	if (num < 10)
		str = "0" + str;
	return str;
}