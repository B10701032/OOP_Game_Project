#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
using namespace std;

bool canOpenFile(string fileName); // 檢查檔案是否存在

bool palyAgainOrNot(); // 決定開起新的遊戲或離開程式

int stringToInteger(string str); // 將 string 轉成 int

string integerToString(int num); // 將 int 轉成 string