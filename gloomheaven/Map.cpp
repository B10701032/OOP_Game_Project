#include "Map.h"

Map::Map()
{
}

Map::Map(string mapFileName) : mapFileName(mapFileName)
{
	ifstream inStream;
	inStream.open(mapFileName);
	if (inStream.fail())
	{
		cout << "error: fail to open " << this->mapFileName << endl
			<< "\tplease exit the program and check the txt file\n\n";
	}
	else
	{
		int row = 0, col = 0;
		inStream >> row >> col;
		inStream.ignore();
		this->rowCnt = row;
		this->colCnt = col;
		this->defaultMap = new char*[row]; // 初始化地圖
		this->map = new char*[row]; // 
		this->visibility = new bool*[row]; // 初始化能見度 (一開始皆為看不見)
		for (int i = 0; i < row; i++)
		{
			this->defaultMap[i] = new char[col];
			this->map[i] = new char[col];
			this->visibility[i] = new bool[col];
			for (int j = 0; j < col; j++)
			{
				this->defaultMap[i][j] = 0;
				this->map[i][j] = 0;
				this->visibility[i][j] = false;
			}
		}

		// 讀地圖
		for (int i = 0; i < row; i++)
		{
			string r;
			getline(inStream, r); // 一次讀取一列
			for (int j = 0; j < col; j++)
			{
				if (r[j] == '0') // 0 直接轉成空白
				{
					this->defaultMap[i][j] = ' ';
					this->map[i][j] = ' ';
				}
				else
				{
					this->defaultMap[i][j] = r[j];
					this->map[i][j] = r[j];
				}
			}
		}

		// 讀起始位置
		string pos;
		getline(inStream, pos);
		istringstream iss(pos);
		int x = 0, y = 0;
		while (iss >> x >> y)
		{
			Point2d p(y, x); // x為column、y為row
			this->initialPosition.push_back(p);
		}
		iss.clear();
		iss.str("");
		sortPosition(); // 將起始位置由小到大排列

		string cntStr;
		getline(inStream, cntStr);
		int cnt = stoi(cntStr);
		this->maxMonsterCnt = cnt;
		for (int i = 0; i < cnt; i++)
		{
			string m;
			getline(inStream, m);
			this->monsterInfo.push_back(m);
		}
	}
	inStream.close();
}

bool Map::getVisibility(Point2d p)
{
	int x = p.getX();
	int y = p.getY();
	return this->visibility[x][y];
}

void Map::display() // 顯示地圖 (選起始位置階段外的地圖)
{
	for (int i = 0; i < this->rowCnt; i++)
	{
		for (int j = 0; j < this->colCnt; j++)
		{
			if (this->visibility[i][j] == true) 
			{
				cout << map[i][j];
			}

			else // 如果區域還沒開啟，一律輸出空白
			{
				cout << " ";
			}
		}
		cout << endl;
	}
} 

void Map::openArea(Point2d p, Point2d door) // 給點，開啟點位於的特定區域
{
	// 左上
	for (int x = p.getX(); x > 0; x--)
	{
		if (isWall(x, p.getY()))
		{
			visibility[x][p.getY()] = true;

			for (int y = p.getY(); y > 0; y--)
			{
				if (defaultMap[x][y] == '1' || defaultMap[x][y] == '2') break;

				visibility[x][y] = true;
				if (defaultMap[x][y] == '3') break;
				//display();
			}

			break;
		}


		for (int y = p.getY(); y > 0; y--)
		{
			visibility[x][y] = true;
			if (isWall(x, y)) break;
			//display();
		}
	}

	// 左下
	for (int x = p.getX(); x < rowCnt; x++)
	{
		if (isWall(x, p.getY()))
		{

			visibility[x][p.getY()] = true;

			for (int y = p.getY(); y > 0; y--)
			{
				if (defaultMap[x][y] == '1' || defaultMap[x][y] == '2') break;

				visibility[x][y] = true;
				if (defaultMap[x][y] == '3') break;
				//display();
			}

			break;
		}

		for (int y = p.getY(); y > 0; y--)
		{
			visibility[x][y] = true;
			if (isWall(x, y)) break;
			//display();
		}
	}

	// 右上
	for (int x = p.getX(); x > 0; x--)
	{
		if (isWall(x, p.getY()))
		{
			visibility[x][p.getY()] = true;

			for (int y = p.getY(); y > 0; y++)
			{
				if (defaultMap[x][y] == '1' || defaultMap[x][y] == '2') break;

				visibility[x][y] = true;
				if (defaultMap[x][y] == '3') break;
				//display();
			}

			break;
		}

		for (int y = p.getY(); y < colCnt; y++)
		{
			visibility[x][y] = true;
			if (isWall(x, y))
			{
				break;
			}
			//display();
		}
	}

	// 右下
	for (int x = p.getX(); x < rowCnt; x++)
	{
		if (isWall(x, p.getY()))
		{
			visibility[x][p.getY()] = true;

			for (int y = p.getY(); y < colCnt; y++)
			{
				if (defaultMap[x][y] == '1' || defaultMap[x][y] == '2') break;

				visibility[x][y] = true;
				if (defaultMap[x][y] == '3') break;
				//display();
			}

			break;
		}

		for (int y = p.getY(); y < colCnt; y++)
		{
			visibility[x][y] = true;
			if (isWall(x, y)) break;
			//display();
		}
	}

	// 開門之後改成普通地板 (這裡改 default map 就好，此時 icon 要留著)
	this->defaultMap[door.getX()][door.getY()] = '1';
}

void Map::clear() // 將所有的能見度改為 false (DEBUG 用)
{
	for (int i = 0; i < this->rowCnt; i++)
		for (int j = 0; j < this->colCnt; j++)
			this->visibility[i][j] = false;
}

void Map::draw(Point2d p, char icon) // 給點，把該點的圖標印到地圖上
{
	this->map[p.getX()][p.getY()] = icon;
}

void Map::erase(Point2d p) // 把該點從地圖上擦掉，還原起始的狀態
{
	this->map[p.getX()][p.getY()] = this->defaultMap[p.getX()][p.getY()];
}

void Map::sortPosition()
{
	if (this->initialPosition.size() > 0)
	{
		for (int i = 0; i < this->initialPosition.size() - 1; i++)
		{
			for (int j = i; j < this->initialPosition.size(); j++)
			{
				if (this->initialPosition[i].getX() >= this->initialPosition[j].getX()) // row 較小的位置較小
				{
					if (this->initialPosition[i].getX() == this->initialPosition[j].getX()) // 如果 row 一樣
					{
						if (this->initialPosition[i].getY() > this->initialPosition[j].getY()) // col 較小的位置較小
						{
							Point2d temp = this->initialPosition[i];
							this->initialPosition[i] = this->initialPosition[j];
							this->initialPosition[j] = temp;
						}
					}
					else
					{
						Point2d temp = this->initialPosition[i];
						this->initialPosition[i] = this->initialPosition[j];
						this->initialPosition[j] = temp;
					}
				}
			}
		}
	}
}

bool Map::canSee(Point2d fr, Point2d to) // 判斷兩點之間有沒有視野
{
	//cout << "check whether one position can see another position\n"; // 測試用
	vector<Point2d> pointAccess;
	Point2d p;
	double m;
	double x1 = fr.getX();
	double y1 = fr.getY();
	double x2 = to.getX();
	double y2 = to.getY();
	double value_y(0);
	bool result = true;

	if (x1 == x2 or y1 == y2) {
		if (x1 == x2) {
			p.setX(x1);
			if (y1 < y2) {
				for (double i = y1; i < y2; i++) {
					p.setY(i);
					pointAccess.push_back(p);
					//std::cout << p.getX() << " , " << p.getY() << endl;
				}
			}
			else {
				for (double i = y2; i < y1; i++) {
					p.setY(i);
					pointAccess.push_back(p);
					//std::cout << p.getX() << " , " << p.getY() << endl;
				}
			}
		}
		else if (y1 == y2) {
			p.setY(y1);
			if (x1 < x2) {
				for (double i = x1; i < x2; i++) {
					p.setX(i);
					pointAccess.push_back(p);
					//std::cout << p.getX() << " , " << p.getY() << endl;
				}
			}
			else {
				for (double i = x2; i < x1; i++) {
					p.setX(i);
					pointAccess.push_back(p);
					//std::cout << p.getX() << " , " << p.getY() << endl;
				}

			}

		}
	}
	else {
		m = (y1 - y2) / (x1 - x2);
		if (x1 < x2) {
			for (double i = x1; i < x2; i += 0.01) {
				value_y = m * (i - x1) + y1;
				p.setX(round(i));
				p.setY(round(value_y));
				pointAccess.push_back(p);
				//std::cout << p.getX() << " , " << p.getY() << endl;
			}

		}
		else {
			for (double i = x2; i < x1; i += 0.01) {
				value_y = m * (i - x1) + y1;
				p.setX(round(i));
				p.setY(round(value_y));
				pointAccess.push_back(p);
				//std::cout << p.getX() << " , " << p.getY() << endl;
			}
		}
	}
	for (int i = 0; i < pointAccess.size(); i++) {
		if (getMap()[pointAccess[i].getX()][pointAccess[i].getY()] == ' ')
		{
			result = false;
			break;
		}
	}
	//if (result) cout << "true" << endl;
	//else cout << "false" << endl;
	return result;
}

int Map::countDistance(Point2d fr, Point2d to) // 計算兩點之間的距離
{
	//cout << "count distance between two points\n"; // 測試用
	int distance = 0;
	distance += abs(fr.getX() - to.getX());
	distance += abs(fr.getY() - to.getY());
	//cout << distance << endl;
	return distance;
}

bool Map::isOpen(Point2d p) {
	if (this->visibility[p.getX()][p.getY()] == true) return true;
	else return false;
}

bool Map::isWall(int x, int y)  // 判斷是否為牆壁
{

	if (x == 0 || x == rowCnt - 1 || y == 0 || y == colCnt - 1) return true;

	if (defaultMap[x][y] == ' ' || defaultMap[x][y] == '3')
	{
		if (defaultMap[x][y - 1] == ' ' || defaultMap[x][y + 1] == ' ') return true;
		if (defaultMap[x - 1][y] == ' ' || defaultMap[x + 1][y] == ' ') return true;
	}

	return false;
}