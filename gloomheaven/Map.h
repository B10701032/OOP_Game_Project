#pragma once
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include "Point2d.h"

using namespace std;

class Map
{
public:
	Map();
	Map(string mapFileName);
	int getRowCnt() { return rowCnt; }
	int getColCnt() { return colCnt; }
	int getMaxMonsterCnt() { return maxMonsterCnt; }
	char** getDefaultMap() { return defaultMap; }
	char** getMap() { return map; }
	bool** getVisibility() { return visibility; }
	bool getVisibility(Point2d p);
	vector<Point2d> getInitialPosition() { return initialPosition; }
	vector<string> getMonsterInfo() { return monsterInfo; }
	void sortPosition();
	void openArea(Point2d p, Point2d door); // 給點，開啟點位於的特定區域
	void display(); // 顯示地圖 (選起始位置階段外的地圖)
	void clear(); // 將所有的能見度改為 false (DEBUG 用)
	void draw(Point2d p, char icon); // 給點，把該點的圖標印到地圖上
	void erase(Point2d p); // 把該點從地圖上擦掉，還原起始的狀態
	bool canSee(Point2d fr, Point2d to); // 判斷兩點之間有沒有視野
	int countDistance(Point2d fr, Point2d to); // 計算兩點之間的距離
	bool isOpen(Point2d p);
	bool isWall(int x, int y); // 判斷是否為牆壁
private:
	string mapFileName;
	int rowCnt;
	int colCnt;
	int maxMonsterCnt; // 地圖有幾隻怪物
	char** defaultMap; // 存原始設定 (只有 0 1 2 3)
	char** map; // 在 cmd 上顯示的地圖
	bool** visibility; // 可以在地圖上看見該格子
	vector<Point2d> initialPosition; // 角色可選擇的起始位置 // 由 row 小到大排
	vector<string> monsterInfo; // 地圖怪物資訊 (建 Monster 參考用)
};
