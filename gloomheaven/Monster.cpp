#include "Monster.h"

Monster::Monster()
{
}

Monster::Monster(string name, string level, int hp, int damage, int range, 
	int handCnt, vector<MonsterSkill> hand, int id, Point2d position)
	: name(name), level(level), hp(hp), damage(damage), range(range), handCnt(handCnt), hand(hand), id(id), position(position)
{
	this->alive = true;
	this->icon = 'a' + id; // 依照在地圖檔案讀到的順序給予代號
	// 初始 handStatus
	handStatus = new int[this->handCnt];
	for (int i = 0; i < handCnt; i++)
		handStatus[i] = 1;
	shield = 0;
}

void Monster::resetHandStatus() // 出現重洗標誌則將棄牌堆洗回去
{
	for (int i = 0; i < handCnt; i++)
		handStatus[i] = 1;
}

