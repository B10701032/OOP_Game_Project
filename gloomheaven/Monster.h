#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include "MonsterData.h"
#include "MonsterSkill.h"
#include "Point2d.h"

using namespace std;

class Monster
{
public:
	Monster();
	Monster(string name, string level, int hp, int damage, int range, 
		int handCnt, vector<MonsterSkill> hand, int id, Point2d position);
	void setSelectedCardId(int n) { this->selectedCardId = n; }
	void setPosition(Point2d newPosition) { this->position = newPosition; }
	void setAgi(int n) { this->agi = n; }
	void setHp(int newHp) { this->hp = newHp; }
	void setShield(int newS) { this->shield = newS; }
	string getName() { return this->name; }
	string getLevel() { return this->level; }
	int getHp() { return this->hp; }
	int getDamage() { return this->damage; }
	int getRange() { return this->range; }
	int getHandCnt() { return this->handCnt; }
	vector<MonsterSkill> getHand() { return this->hand; }
	int getId() { return this->id; }
	char getIcon() { return this->icon; }
	bool isAlive() { return this->alive; }
	int* getHandStatus() { return handStatus; }
	Point2d getPosition() { return position; }
	int getSelectedCardId() { return this->selectedCardId; }
	int getAgi(){ return this->agi; }
	int getShield() { return this->shield; }
	void isAttacked(int val) { this->hp -= val; }
	void isKilled() { this->alive = false; }
	void useCard(int id) { this->handStatus[id] = 0; }
	void resetHandStatus(); // 出現重洗標誌則將棄牌堆洗回去
private:
	string name;
	string level;
	int hp;
	int damage;
	int range;
	int handCnt;
	vector<MonsterSkill> hand;
	int id;
	char icon;
	bool alive;
	int* handStatus;
	Point2d position;
	int selectedCardId; // 每一輪決定要打的牌
	int agi; // 怪獸每一輪的敏捷值
	int shield; // 怪獸每一輪的護甲值
};