#include "MonsterData.h"

MonsterData::MonsterData()
{
}

MonsterData::MonsterData(string name, int baseHp, int baseDamage, int baseRange,
	int eliteHp, int eliteDamage, int eliteRange, vector<MonsterSkill> allCards)
	: name(name), baseHp(baseHp), baseDamage(baseDamage), baseRange(baseRange),
	eliteHp(eliteHp), eliteDamage(eliteDamage), eliteRange(eliteRange), allCards(allCards)
{
	allCardCnt = 6; // 怪物有 6 張技能卡
}