#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "MonsterSkill.h"
using namespace std;

class MonsterData
{
public:
	MonsterData();
	MonsterData(string name, int baseHp, int baseDamage, int baseRange,
		int eliteHp, int eliteDamage, int eliteRange, vector<MonsterSkill> allCards);
	string getName() { return name; }
	int getBaseHp() { return baseHp; }
	int getBaseDamage() { return baseDamage; }
	int getBaseRange() { return baseRange; }
	int getEliteHp() { return eliteHp; }
	int getEliteDamage() { return eliteDamage; }
	int getEliteRange() { return eliteRange; }
	int getAllCardCnt() { return allCardCnt; }
	vector<MonsterSkill> getAllCards() { return allCards; }
private:
	string name;
	int baseHp;
	int baseDamage;
	int baseRange;
	int eliteHp;
	int eliteDamage;
	int eliteRange;
	int allCardCnt;
	vector<MonsterSkill> allCards;
};

