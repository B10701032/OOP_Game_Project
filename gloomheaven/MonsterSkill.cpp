#include "MonsterSkill.h"

MonsterSkill::MonsterSkill() : cardId(0), agility(0), skillsCnt(0), reusable(true)
{
}

MonsterSkill::MonsterSkill(string cardDescription)
{
	istringstream iss(cardDescription);
	string tempName, tempId, tempAgi,tempMark;
	iss >> tempName >> tempId >> tempAgi;
	iss.clear();
	iss.str("");
		
	tempMark = cardDescription[cardDescription.length() - 1]; // 最後一個字母

	this->name = tempName;
	this->cardId = stringToInteger(tempId);
	this->agility = stringToInteger(tempAgi);
	if (tempMark == "r") this->reusable = true;
	else this->reusable = false;

	// 技能敘述個數未知，分開處理
	size_t agiPos = cardDescription.find(tempAgi);
	string skillsStr = cardDescription.substr(agiPos + 3); // 擷取含有技能敘述的部分
	skillsStr = skillsStr.substr(0, skillsStr.length() - 1); // 去掉重洗標誌
	setSkillsCnt(skillsStr);
	setSkills(skillsStr);
}

void MonsterSkill::setSkillsCnt(string str) // 計算有幾個技能敘述
{
	int cnt = 0;

	size_t movePos = str.find("move");
	while (movePos != string::npos)
	{
		cnt += 1;
		movePos = str.find("move", movePos + 4);
	}

	size_t attackPos = str.find("attack");
	while (attackPos != string::npos)
	{
		cnt += 1;
		attackPos = str.find("attack", attackPos + 6);
	}

	size_t shieldPos = str.find("shield");
	while (shieldPos != string::npos)
	{
		cnt += 1;
		shieldPos = str.find("shield", shieldPos + 6);
	}

	size_t healPos = str.find("heal");
	while (healPos != string::npos)
	{
		cnt += 1;
		healPos = str.find("heal", healPos + 4);
	}

	this->skillsCnt = cnt;
}

void MonsterSkill::setSkills(string str)
{
	istringstream iss(str);
	string tempWord;
	
	while (iss >> tempWord)
	{
		string tempSkill;

		while (tempWord == "attack") // 避免連續多個 attack 敘述
		{
			tempSkill += tempWord;
			iss >> tempWord;
			tempSkill += " " + tempWord;

			
			if (iss >> tempWord)
			{
				if (tempWord == "range") // attack x range y
				{
					tempSkill += " " + tempWord;
					iss >> tempWord;
					tempSkill += " " + tempWord;
					this->skills.push_back(tempSkill);
				}

				else // 下一個 word 是 move / shield / heal
				{
					this->skills.push_back(tempSkill);
					tempSkill = "";
				}
			}

			else // 如果已經讀到尾端
			{
				this->skills.push_back(tempSkill);
				tempWord = "";
			}
		}

		if (tempWord == "move" || tempWord == "shield" || tempWord == "heal")
		{
			tempSkill += tempWord;
			iss >> tempWord;
			tempSkill += " " + tempWord;
			this->skills.push_back(tempSkill);
		}
	}

	iss.clear();
	iss.str("");
}

void MonsterSkill::print()
{
	cout << this->name << " " << this->cardId << " " << this->agility << " ";
	for (int i = 0; i < this->skillsCnt; i++)
		cout << skills[i] << " ";
	if (reusable)
		cout << "r" << endl;
	else
		cout << "d" << endl;
}
