#pragma once
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include "HelperFunction.h" // stringToInt()
using namespace std;

class MonsterSkill
{
public:
	MonsterSkill();
	MonsterSkill(string cardDescription);
	void setSkillsCnt(string str);
	void setSkills(string str);
	string getName() { return name; }
	int getCardId() { return cardId; }
	int getAgility() { return agility; }
	int getSkillsCnt() { return skillsCnt; }
	vector<string> getSkills() { return skills; }
	bool getReusable() { return reusable; }
	void print();
private:
	string name;
	int cardId;
	int agility;
	int skillsCnt; // 此技能卡有幾個敘述
	vector<string> skills; // 技能敘述的集合
	bool reusable;
};