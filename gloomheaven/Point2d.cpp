#include "Point2d.h"

Point2d::Point2d() : x(0), y(0)
{
}

Point2d::Point2d(int x, int y) : x(x), y(y)
{
}

Point2d::Point2d(const Point2d& other)
{
	this->x = other.x;
	this->y = other.y;
}

double Point2d::getDistance(Point2d other)
{
	double ss = 0;
	ss = pow((this->x - other.x), 2) + pow((this->y - other.y), 2); // sum of square
	return pow(ss, 0.5);
}

Point2d& Point2d::operator=(const Point2d& other)
{
	if (this != &other)
	{
		this->x = other.x;
		this->y = other.y;
	}
	return *this;
}

bool Point2d::operator==(const Point2d& other)
{
	if (this->x == other.x && this->y == other.y)
		return true;
	else
		return false;
}

bool Point2d::operator!=(const Point2d& other)
{
	return !(*this == other);
}