#pragma once
#include <iostream>
#include <cmath>
using namespace std;

class Point2d
{
public:
	Point2d();
	Point2d(int x, int y);
	Point2d(const Point2d& other);
	int getX() { return x; }
	int getY() { return y; }
	void setX(int newX) { x = newX; }
	void setY(int newY) { y = newY; }
	void setXY(int newX, int newY) { x = newX; y = newY; }
	double getDistance(Point2d other);
	Point2d& operator=(const Point2d& other);
	bool operator==(const Point2d& other);
	bool operator!=(const Point2d& other);
private:
	int x; // x �y��
	int y; // y �y��
};