#include <iostream>
#include <string>
#include "Game.h"
#include "Map.h"
#include "Point2d.h"
#include "HelperFunction.h"
using namespace std;

int main(int argc, char *argv[])
{
	// 透過程式執行參數輸入
	string characterFileName = argv[1]; // 角色資料檔案名稱
	string monsterFileName = argv[2]; // 怪物行動卡表檔案
	bool debugMode = false; // DEBUG_MODE
	if (argc == 4 || argc == 6) debugMode = true; // 有無加 input.txt 和 output.txt
	// arge 從 1 開始算，但 argv 從 0 開始數

	////// 在 VS debug 用 
	//string characterFileName = "character1.txt"; // 角色資料檔案名稱
	//string monsterFileName = "monster1.txt"; // 怪物行動卡表檔案
	//bool debugMode = true; // DEBUG_MODE

	if (!canOpenFile(characterFileName)) // 檢查檔名是否存在
	{
		cout << "error: fail to open " << characterFileName << endl
			<< "\tplease check the file name" << endl;
	}

	else if (!canOpenFile(monsterFileName))
	{
		cout << "error: fail to open " << monsterFileName << endl
			<< "\tplease check the file name" << endl;
	}

	else
	{
		bool playAgain = palyAgainOrNot(); // 決定開起新的遊戲或離開程式
		while (playAgain)
		{
			Game game(characterFileName, monsterFileName, debugMode);
			game.startGame(); // 開啟新的一場遊戲
			//game.startGameTest(); // DEBUG 用
			playAgain = palyAgainOrNot();
		}
	}

	return 0;
}

